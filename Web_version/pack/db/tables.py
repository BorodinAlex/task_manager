from sqlalchemy.ext.declarative import declarative_base
import datetime
from sqlalchemy import *
from pack.setting import *


Base = declarative_base()
engine = create_engine(connect_str_to_db())


class Task(Base):
    __tablename__ = 'tasks'

    id = Column(Integer, primary_key=True)
    owner = Column(String, default='None')
    header = Column(String)
    priority = Column(String)
    tags = Column(String)
    comment = Column(String)
    date_of_create = Column(String, default=datetime.datetime.now().date())
    time_of_create = Column(String, default=datetime.datetime.now().time())
    date_of_start = Column(String)
    time_of_start = Column(String)
    date_of_end = Column(String)
    time_of_end = Column(String)
    status = Column(String)
    expert = Column(String, default='None')
    is_linked = Column(String, default='None')
    linked_task_id = Column(String, default='None')
    is_under_task = Column(String, default='None')
    parent_task_id = Column(Integer, default=0)
    is_parent_task = Column(String, default='None')
    under_task_id = Column(String, default='None')


class User(Base):
    __tablename__ = 'users'

    login = Column(String, primary_key=True)
    password = Column(String)
    question = Column(String)
    answer = Column(String)


Base.metadata.create_all(engine)


def create_tables():
    Base.metadata.create_all(engine)


if __name__ == '__main__':
    create_tables()