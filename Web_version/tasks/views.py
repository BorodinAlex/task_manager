from django.shortcuts import render
from pack.methods.task_methods import *
from pack.functions.task_functions import *
from tasks.forms import *
from datetime import datetime, timedelta
from accounts.views import *
from pack.db.tables import User
from sqlalchemy.exc import *
from itertools import groupby

def split_date(date):
    old_date = date.split("-")
    new_date = old_date[2] + '-' + old_date[1] + '-' + old_date[0]
    return new_date


def add_task(request):
    errors = []
    if request.method == 'POST':
        form = TaskForm(request.POST)
        if form.is_valid():
            header = form.cleaned_data.get('header')
            priority = form.cleaned_data.get('priority')
            tags = form.cleaned_data.get('tags')
            comment = form.cleaned_data.get('comment')
            date_of_start = form.cleaned_data.get('date_of_start')
            time_of_start = form.cleaned_data.get('time_of_start')
            date_of_end = form.cleaned_data.get('date_of_end')
            time_of_end = form.cleaned_data.get('time_of_end')
            new_date_of_start = split_date(str(date_of_start))
            new_date_of_end = split_date(str(date_of_end))
            status = form.cleaned_data.get('status')
            user = user_name_and_password()
            if date_of_end < date_of_start:
                errors.append('Дата окончания не должна раньше позже даты начала')
            else:
                new_task = Task(owner=user[0], header=header, priority=priority, tags=tags, comment=comment,
                                date_of_start=new_date_of_start, time_of_start=time_of_start,
                                date_of_end=new_date_of_end, time_of_end=time_of_end, status=status)
                TaskFunctions.add_task(new_task)
                return redirect("main_page")
    else:
        form = TaskForm()
    return render(request, 'tasks_html/add_task.html', {'form': form,
                                                        'errors': errors})


def parse_date(date):
    old_date = date.split("-")
    new_date = old_date[2] + '-' + old_date[1] + '-' + old_date[0]
    return new_date


def change_task(request, pk):
    user_login = user_name_and_password()
    errors = []
    task = TaskFunctions.get_task_by_id(pk)
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            header = form.cleaned_data.get('header')
            priority = form.cleaned_data.get('priority')
            tags = form.cleaned_data.get('tags')
            comment = form.cleaned_data.get('comment')
            date_of_start = form.cleaned_data.get('date_of_start')
            time_of_start = form.cleaned_data.get('time_of_start')
            date_of_end = form.cleaned_data.get('date_of_end')
            time_of_end = form.cleaned_data.get('time_of_end')
            new_date_of_start = split_date(str(date_of_start))
            new_date_of_end = split_date(str(date_of_start))
            status = form.cleaned_data.get('status')
            if date_of_end < date_of_start:
                errors.append('Дата окончания не должна раньше позже даты начала')
            else:
                new_task = Task(id=pk, header=header, priority=priority, tags=tags, comment=comment,
                                date_of_start=new_date_of_start, time_of_start=time_of_start,
                                date_of_end=new_date_of_end, time_of_end=time_of_end, status=status)
                TaskFunctions.change_task(new_task)
                TaskFunctions.change_date_time_task(new_task)
                TaskChangeMethods.change_status(user_login[0],user_login[1], pk, status)
                return redirect('main_page')
    else:
        form = TaskForm(initial={'header': task.header, 'priority': task.priority, 'tags': task.tags,
                                 'comment': task.comment, 'date_of_start': parse_date(task.date_of_start),
                                 'time_of_start': task.time_of_start, 'date_of_end': parse_date(task.date_of_end),
                                 'time_of_end': task.time_of_end})
    return render(request, 'tasks_html/change_task.html', {'form': form,
                                                           'task_pk': pk})


def main_page(request):
    user_login = user_name_and_password()
    tasks = TaskFunctions.get_user_tasks(user_login[0])
    count_of_task = len(tasks)
    count_of_complete_task = 0
    count_of_not_complete_task = 0
    count_of_in_progress_task = 0
    tasks_today = list()
    tasks_week = list()
    tasks_mounth = list()
    for task in tasks:
        if task.status == 'Выполнено':
            count_of_complete_task += 1
        if task.status == 'В ожиданни выполнения':
            count_of_in_progress_task += 1
        if task.status == 'Не выполнено':
            count_of_not_complete_task += 1
        if datetime.strptime(task.date_of_start, '%d-%m-%Y') < datetime.now() < datetime.strptime(task.date_of_end, '%d-%m-%Y'):
            tasks_today.append(task.header)
        if datetime.strptime(task.date_of_start, '%d-%m-%Y') < datetime.now() + timedelta(days=7) < datetime.strptime(task.date_of_end, '%d-%m-%Y'):
            tasks_week.append(task.header)
        if datetime.strptime(task.date_of_start, '%d-%m-%Y') < datetime.now() + timedelta(days=30) < datetime.strptime(task.date_of_end, '%d-%m-%Y'):
            tasks_mounth.append(task.header)

    return render(request, 'main_page.html', {'count_of_task': count_of_task,
                                              'count_of_complete_task': count_of_complete_task,
                                              'count_of_not_complete_task': count_of_not_complete_task,
                                              'count_of_in_progress_task': count_of_in_progress_task,
                                              'tasks_today': tasks_today,
                                              'tasks_week': tasks_week,
                                              'tasks_mounth': tasks_mounth,
                                              'user_login': user_login[0]})


# def plan_on_day(request):
#     user_login = user_name_and_password()
#     tasks_today = []
#     tasks = TaskFunctions.get_user_tasks(user_login[0])
#     for task in tasks:
#         if datetime.strptime(task.date_of_start, '%d-%m-%Y') < datetime.now() < datetime.strptime(task.date_of_end, '%d-%m-%Y'):
#             tasks_today.append(task.header)
#     return render(request, 'plan_on_day.html', {'tasks': tasks_today})

def plan_on_day(request):
    user_login = user_name_and_password()
    tasks = TaskFunctions.get_user_tasks(user_login[0])
    return render(request, 'plan_on_day.html', {'tasks': tasks})


def look_at_task(request, pk):
    task = TaskFunctions.get_task_by_id(pk)
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            pass
    else:
        form = TaskForm(initial={'id': pk, 'header': task.header, 'priority': task.priority, 'tags': task.tags,
                                 'comment': task.comment, 'date_of_start': parse_date(task.date_of_start),
                                 'time_of_start': task.time_of_start, 'date_of_end': parse_date(task.date_of_end),
                                 'time_of_end': task.time_of_end})
    return render(request, 'tasks_html/look_task.html', {'form': form})


def look_at_task_dt(request, pk):
    task = TaskFunctions.get_task_by_id(pk)
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            pass
    else:
        form = TaskForm(initial={'id': pk, 'header': task.header, 'priority': task.priority, 'tags': task.tags,
                                 'comment': task.comment, 'date_of_start': parse_date(task.date_of_start),
                                 'time_of_start': task.time_of_start, 'date_of_end': parse_date(task.date_of_end),
                                 'time_of_end': task.time_of_end})
    return render(request, 'tasks_html/look_task_dt.html', {'form': form})


def delete_task(request, pk):
    user_login = user_name_and_password()
    try:
        TaskDeleteMethods.delete_task(user_login[0], user_login[1], pk)
    except:
        pass
    return redirect('main_page')


def change_status_of_task(request, pk, status):
    user_login = user_name_and_password()
    TaskChangeMethods.change_status(user_login[0], user_login[1], pk, status)
    return redirect('plan_on_day')


def all_tasks(request):
    user_login = user_name_and_password()
    tasks = TaskFunctions.get_user_tasks(user_login[0])
    return render(request, 'tasks_html/all_tasks.html', {'tasks': tasks})


def create_list_of_users_tasks(login):
    tasks = TaskFunctions.get_user_tasks(login)
    list_of_tasks = list()
    for task in tasks:
        list_of_tasks.append(task.id)
    print(list_of_tasks)
    return list_of_tasks


def split_str(string):
    try:
        new_split = string.split(',')
        new_split = list(filter(None, new_split))
        return new_split
    except:
        return None


def delete_link_between_task(request, left_link_id, right_link_id):
    user_login = user_name_and_password()
    LinkedTaskMethods.delete_link_between_linked_task(user_login[0], user_login[1], left_link_id, right_link_id)
    return redirect('main_page')


def linked_action(request, pk):
    errors = []
    login = user_name_and_password()
    task = TaskFunctions.get_task_by_id(pk)
    linked_ids = split_str(task.linked_task_id)
    if request.method == "POST":
        form = LinkedTasksForm(request.POST)
        if form.is_valid():
            id = form.cleaned_data.get('id')
            linked_task = TaskFunctions.get_task_by_id(id)
            is_task_already_linked = HelpMethods.is_tasks_already_linked(id, linked_task.id)
            if id == pk:
                errors.append("Вы не можете связать задачу саму с собой")
            elif linked_task is None:
                errors.append("Указанной вами задачи не существует")
            elif linked_task.owner != login[0]:
                errors.append("Вы не можете связывать вашу задачу с задачей, которая вам не пренадлежит")
            elif is_task_already_linked is True:
                errors.append("Задачи уже связаны")
            else:
                LinkedTaskMethods.add_linked_task(login[0], login[1], pk, id)
                return redirect('main_page')
    else:
        form = LinkedTasksForm()
    return render(request, 'tasks_html/linked_actions.html', {'form': form,
                                                              'task_pk': pk,
                                                              'is_linked': task.is_linked,
                                                              'linked_id': linked_ids,
                                                              'errors': errors,
                                                              'left_id': pk})


def delete_parent_child_link(request, id, parent):
    user_login = user_name_and_password()
    ParentTaskMethods.delete_link_between_task(user_login[0], user_login[1], id, parent)
    return redirect('main_page')


def parent_actions(request, pk):
    errors = []
    under_tasks = ()
    child_task = TaskFunctions.get_task_by_id(pk)
    under_tasks = split_str(child_task.under_task_id)
    login = user_name_and_password()
    if request.method == "POST":
        form = ParentTaskForm(request.POST)
        if form.is_valid():
            parent_task_id = form.cleaned_data.get('parent_task_id')
            expert = form.cleaned_data.get('expert')
            is_expert_exist = UsersFunctions.get_user_by_login(expert)
            parent_task = TaskFunctions.get_task_by_id(parent_task_id)
            is_child_has_parent_task = HelpMethods.is_task_have_parent(parent_task_id)
            is_tasks_already_linked = HelpMethods.check_under_task_and_parent_task_linked(parent_task_id, pk)
            ######################
            if parent_task is None:
                errors.append("Такой задачи не существует")
            elif parent_task.is_parent_task == "Yes":
                errors.append("Эта задача уже является родителем")
            elif pk == parent_task_id:
                errors.append("Нельзя объявить родителем задачи саму задачу")
            elif is_expert_exist is None:
                errors.append("Введенного вами пользователя не существует")
            elif is_child_has_parent_task:
                errors.append("У задачи уже есть родитель, сначала удалите текущего родителя")
            elif is_tasks_already_linked:
                errors.append("Данные задачи уже связаны сущностью 'родитель-ребенок'")
            else:
                ParentTaskMethods.add_parent_for_task(login[0], login[1], pk, parent_task_id, expert)
                return redirect('main_page')
    else:
        form = ParentTaskForm()
    return render(request, 'tasks_html/parent_actions.html', {'form': form,
                                                              'under_tasks': under_tasks,
                                                              'is_parent': child_task.is_parent_task,
                                                              'is_under_task': child_task.is_under_task,
                                                              'parent_id': child_task.parent_task_id,
                                                              'expert': child_task.expert,
                                                              'errors': errors,
                                                              'pk': pk})
