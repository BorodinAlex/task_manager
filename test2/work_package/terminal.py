#!/usr/bin/env python3
import sys
import click

from work_package import *


@click.command()
@click.argument('func', nargs=1)
@click.argument('args', nargs=-1)
def main(func, args):
    try:
        ALL_FUNCTIONS[func](*args)
    except Exception as exc:
        print("An error occured:", exc)


if __name__ == '__main__':
    main(help_en, )
