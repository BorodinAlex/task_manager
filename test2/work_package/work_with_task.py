import psycopg2
import logging
import datetime
import work_package.work_with_db as work_with_db
import work_package.work_with_users as work_with_users
logging.basicConfig(filename="journal.log", level=logging.INFO)


def insert_task_into_db(header, tags, comment, date_of_start, date_of_end, status):
    '''
    adding a new single task to the database
    :param header: the name of the task
    :param tags: tag / task group
    :param comment: comment to the task
    :param date_of_start: the start date of the alert
    :param date_of_end: the end date of the notification
    :param status: is the current status of the task.Can be set to 'Done', 'Not performed', 'Pending execution'
    :return: single task in db
    '''
    try:
        if work_with_users.user_name_is() == '':
            print("You are not logged in. Use the login command ('Your login', 'Your password') to enter the system.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            if datetime.datetime.strptime(date_of_start, "%d.%m.%Y") > datetime.datetime.strptime(date_of_end, "%d.%m.%Y"):
                print("Start date must not be after the end date")
            else:
                owner = work_with_users.user_name_is()
                cursor.execute("insert into tasks (owner, header, tags, comment, date_of_start, date_of_end, status) "
                               "values(%s,%s,%s,%s,%s,%s,%s)", (owner, header, tags, comment,
                                                                date_of_start, date_of_end, status))
                conn.commit()
                print('Note {0} successfully added!'.format(header, ))
                logging.info(" %s: Successful insert task by %s " % (datetime.datetime.now(),
                                                                     work_with_users.user_name_is()))

            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)
    except:
        print("You can not add a single task, please check the connection settings to the database"
              "or the correctness of the arguments sent.")
        logging.warning(" %s: Error with insert task by %s" % (datetime.datetime.now(), work_with_users.user_name_is()))


def update_task_in_db(header, tags, comment, date_of_start, date_of_end, status, pk):
    '''
    update's single task
    :param header: the name of the task
    :param tags: tag / task group
    :param comment: comment to the task
    :param date_of_start: the start date of the alert
    :param date_of_end: the end date of the notification
    :param status: is the current status of the task.Can be set to 'Done', 'Not performed', 'Pending execution'
    :param pk: personal key of the single task
    :return: updated single task
    '''
    try:
        if work_with_users.user_name_is() == '':
            print("You are not logged in. Use the login command ('Your login', 'Your password') to enter the system.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            if datetime.datetime.strptime(date_of_start, "%d.%m.%Y") > datetime.datetime.strptime(date_of_end, "%d.%m.%Y"):
                print("Start date must not be after the end date")
            else:
                login = work_with_users.user_name_is()
                cursor.execute("UPDATE tasks SET header=%s, tags=%s, comment=%s, date_of_start=%s, date_of_end=%s, status=%s"
                               "WHERE id = %s and owner = %s", (header, tags, comment, date_of_start,
                                                                date_of_end, status, pk, login))
                conn.commit()
                work_with_db.print_for_alerts_of_tasks(pk, 'изменена')
                logging.info(" %s: Successful update task by %s " % (datetime.datetime.now(),
                                                                     work_with_users.user_name_is()))

            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)
    except:
        print("You can not update a single task, please check the connection settings to the database"
              "or the correctness of the arguments sent.")
        logging.warning(" %s: Error with update task by %s" % (datetime.datetime.now(),
                                                               work_with_users.user_name_is()))


def delete_task_from_db(pk): ####
    '''
    delete single task from db
    :param pk: personal key of the single task
    :return: delete single task
    '''
    try:
        if work_with_users.user_name_is() == '':
            print("You are not logged in. Use the login command ('Your login', 'Your password') to enter the system.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            login = work_with_users.user_name_is()
            cursor.execute("DELETE FROM tasks WHERE id = %s and owner=%s", (pk, login))
            conn.commit()

            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)
            print("The node with id = {0} was successfully deleted".format(pk))
            logging.info(" %s: Successful delete task by %s " % (datetime.datetime.now(),
                                                                 work_with_users.user_name_is()))
    except:
        print("You can not delete a single task, please check the connection settings to the database"
              "or the correctness of the arguments sent.")
        logging.warning(" %s: Error with delete task by %s" % (datetime.datetime.now(),
                                                               work_with_users.user_name_is()()))


# def print_task():
#     '''
#     prints single tasks
#     :return: single tasks
#     '''
#     try:
#         if work_with_users.user_name_is() == '':
#             print("You are not logged in. Use the login command ('Your login', 'Your password') to enter the system.")
#         else:
#             conn = work_with_db.connect_to_db()
#             cursor = conn.cursor()
#
#             login = work_with_users.user_name_is()
#             cursor.execute("SELECT * FROM tasks where owner = %s and (status='Not done' "
#                            "or status='Pending execution')", (login, ))
#             row = cursor.fetchone()
#
#             work_with_db.print_list_of_tasks_data(row, cursor)
#             work_with_db.close_cursor(cursor)
#             work_with_db.close_connect(conn)
#             logging.info(" %s: Successful print task for %s " % (datetime.datetime.now(),
#                                                                  work_with_users.user_name_is()))
#     except:
#         print("You can not print a single tasks, please check the connection settings to the database"
#               "or the correctness of the arguments sent.")
#         logging.warning(" %s: Error with print task for %s" % (datetime.datetime.now(),
#                                                                work_with_users.user_name_is()))

def print_task():
    '''
        prints single tasks
        :return: single tasks
        '''
    try:
        if work_with_users.user_name_is() == '':
            print("You are not logged in. Use the login command ('Your login', 'Your password') to enter the system.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            login = work_with_users.user_name_is()
            cursor.execute("SELECT * FROM tasks where owner = %s and (status='В ожидании выполнения' "
                           "or status='Pending execution')", (login, ))
            row = cursor.fetchone()

            work_with_db.print_list_of_tasks_data(row, cursor)
            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)
            logging.info(" %s: Successful print task for %s " % (datetime.datetime.now(),
                                                                 work_with_users.user_name_is()))
    except:
        print("You can not print a single tasks, please check the connection settings to the database"
              "or the correctness of the arguments sent.")
        logging.warning(" %s: Error with print task for %s" % (datetime.datetime.now(),
                                                               work_with_users.user_name_is()))


def print_archived_task():
    '''
        returns single tasks from the archive
        :return: single tasks from the archive
        '''
    try:
        if work_with_users.user_name_is() == '':
            print("You are not logged in. Use the login command ('Your login', 'Your password') to enter the system.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            login = work_with_users.user_name_is()
            cursor.execute("SELECT * FROM tasks where owner = %s and (status='Done' or status='overdue')", (login, ))
            row = cursor.fetchone()

            work_with_db.print_list_of_tasks_data(row, cursor)
            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)
            logging.info(" %s: Successful print  task for %s " % (datetime.datetime.now(),
                                                                 work_with_users.user_name_is()))
    except:
        print("You can not print archive single task, please check the connection settings to the database"
              "or the correctness of the arguments sent.")
        logging.warning(" %s: Error with print task for %s" % (datetime.datetime.now(),
                                                               work_with_users.user_name_is()))


# def print_archived_task():
#     '''
#     returns single tasks from the archive
#     :return: single tasks from the archive
#     '''
#     try:
#         if work_with_users.user_name_is() == '':
#             print("You are not logged in. Use the login command ('Your login', 'Your password') to enter the system.")
#         else:
#             conn = work_with_db.connect_to_db()
#             cursor = conn.cursor()
#
#             login = work_with_users.user_name_is()
#             cursor.execute("SELECT * FROM tasks where owner = %s and (status='Выполнено')", (login, ))
#             row = cursor.fetchone()
#
#             work_with_db.print_list_of_tasks_data(row, cursor)
#             work_with_db.close_cursor(cursor)
#             work_with_db.close_connect(conn)
#             logging.info(" %s: Successful print  task for %s " % (datetime.datetime.now(),
#                                                                  work_with_users.user_name_is()))
#     except:
#         print("You can not print archive single task, please check the connection settings to the database"
#               "or the correctness of the arguments sent.")
#         logging.warning(" %s: Error with print task for %s" % (datetime.datetime.now(),
#                                                                work_with_users.user_name_is()))



def group_by_for_tasks(column):
    '''

    single task grouping
    :param column: the parameter by which the grouping takes place
    :return: grouped single tasks
        '''
    try:
        if work_with_users.user_name_is() == '':
            print("You are not logged in. Use the login command ('Your login', 'Your password') to enter the system.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            login = work_with_users.user_name_is()
            cursor.execute("SELECT * from tasks where owner =%s ORDER BY "+column+"", (login, ))
            row = cursor.fetchone()

            work_with_db.print_list_of_tasks_data(row, cursor)
            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)
            logging.info(" %s: Successful grouped task by %s " % (datetime.datetime.now(),
                                                                  work_with_users.user_name_is()))
    except:
        print("You can not group a single tasks, please check the connection settings to the database"
              "or the correctness of the arguments sent.")
        logging.warning(" %s: Error with grouped task by %s" % (datetime.datetime.now(),
                                                                work_with_users.user_name_is()))


def search_by_header_in_tasks(search_info):
    '''
        search by name
        :param search_info: the parameter by which the search takes place
        :return: search results
        '''
    try:
        if work_with_users.user_name_is() == '':
            print("You are not logged in. Use the login command ('Your login', 'Your password') to enter the system.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            login = work_with_users.user_name_is()
            cursor.execute("SELECT * from tasks WHERE header like %s and owner = %s",
                           (search_info, login))
            row = cursor.fetchone()

            work_with_db.print_list_of_tasks_data(row, cursor)
            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)
            logging.info(" %s: Successful search task by %s " % (datetime.datetime.now(),
                                                                 work_with_users.user_name_is()))
    except:
        print("You can not perform a search, please check the connection settings to the database or "
              "the correctness of the arguments sent.")
        logging.warning(" %s: Error with search task by %s" % (datetime.datetime.now(),
                                                               work_with_users.user_name_is()))


def search_by_status_in_tasks(search_info):
    '''
    search by status
    :param search_info: the parameter by which the search takes place
    :return: search results
    '''
    try:
        if work_with_users.user_name_is() == '':
            print("You are not logged in. Use the login command ('Your login', 'Your password') to enter the system.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            login = work_with_users.user_name_is()
            cursor.execute("SELECT * from tasks WHERE status like %s and owner = %s",
                           (search_info, login))
            row = cursor.fetchone()

            work_with_db.print_list_of_tasks_data(row, cursor)
            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)
            logging.info(" %s: Successful search task by %s " % (datetime.datetime.now(),
                                                                 work_with_users.user_name_is()))
    except:
        print("You can not perform a search, please check the connection settings to the database or "
              "the correctness of the arguments sent.")
        logging.warning(" %s: Error with search task by %s" % (datetime.datetime.now(),
                                                               work_with_users.user_name_is()))


def change_status_task(pk, status):
    '''
        changes the status of the single task
        :param pk: single task personal key
        :param status: the status to which we change
        :return: changed single task status
        '''
    try:
        if work_with_users.user_name_is() == '':
            print("You are not logged in. Use the login command ('Your login', 'Your password') to enter the system.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            login = work_with_users.user_name_is()
            cursor.execute("UPDATE tasks SET status = %s WHERE id = %s and owner = %s ", (status, pk, login, ))
            conn.commit()
##################################
            work_with_db.print_for_alerts_of_tasks(pk, 'changed')
            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)
            logging.info(" %s: Successful change status task by %s " % (datetime.datetime.now(),
                                                                                    work_with_users.user_name_is()))
    except:
        print("You can not change the status of the task, please check the connection settings to "
              "the database or the correctness of the arguments sent..")
        logging.warning(" %s: Error with change status task by %s" % (datetime.datetime.now(),
                                                                                  work_with_users.user_name_is()))
