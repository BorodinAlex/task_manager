import work_package.work_with_users as work_with_users
import work_package.work_with_db as work_with_db
import work_package.work_with_under_group_tasks as work_with_under_group_tasks
import work_package.work_with_group_tasks as work_with_group_tasks
import work_package.work_with_task as work_with_task
import unittest

'''
Here are the unit tests for our project
'''

def main():
    test_registration()
    test_reset_password()
    test_login()
    test_change_password()
    test_insert_task_into_db()
    test_update_task_in_db()
    test_change_status_task()
    test_delete_task_from_db()
    test_insert_group_task_into_db()
    test_update_group_task_in_db()
    test_change_status_group_task()
    test_insert_under_group_task_into_db()
    test_update_under_group_task_in_db()
    test_change_status_under_group_task()
    test_delete_group_task_from_db()


def test_registration():
    try:
        work_with_users.registration('UserName1', 'UserPassword1234', 'Your name', 'Alex')
    except:
        print("Регистрация пользователей неисправна")

def test_reset_password():
    try:
        work_with_users.reset_password('UserName1', 'Your name', 'Alex', 'UserPassword4321')
    except:
        print("Восстановление пароля неисправно")

def test_login():
    try:
        work_with_users.login('UserName1', 'UserPassword4321')
    except:
        print("Функция login неисправна")

def test_change_password():
    try:
        work_with_users.change_password('Your name', 'Alex', 'UserPassword1234')
    except:
        print("Изменение пароля неисправно")

def test_insert_task_into_db():
    try:
        work_with_task.insert_task_into_db('Задача 1', 'Группа1', 'Комментарий1', '11.11.2018', '15.11.2018', 'Не выполнено')
    except:
        print("Добавление задач неисправно")

def test_update_task_in_db():
    try:
        work_with_task.update_task_in_db('Задача 1', 'Группа1', 'Комментарий1', '11.11.2018', '15.11.2018', 'Не выполнено',1)
    except:
        print("Изменение задач неисправно")

def test_change_status_task():
    try:
        work_with_task.change_status_task(1, 'Выполнено')
    except:
        print("Изменение статуса задач неисправно")

def test_delete_task_from_db():
    try:
        work_with_task.delete_task_from_db(1)
    except:
        print("Удаление статуса задач неисправно")

def test_insert_group_task_into_db():
    try:
        work_with_group_tasks.insert_group_task_into_db('Групповая задача 1', 'Группа 1','Комментарий','11.11.2018', '15.11.2018', 'Не выполнено')
    except:
        print("Добавление групповой задачи неисправно")

def test_update_group_task_in_db():
    try:
        work_with_group_tasks.update_group_task_in_db('Групповая задача 2', 'Группа 2','Комментарий2','11.11.2018', '18.11.2018', 'Не выполнено',1)
    except:
        print("Редактирование групповой задачи неисправно")

def test_change_status_group_task():
    try:
        work_with_group_tasks.change_status_group_task(1, 'Выполнено')
    except:
        print("Изменение статуса групповой задачи неисправно")

def test_insert_under_group_task_into_db():
    try:
        work_with_under_group_tasks.insert_under_group_task_into_db(1, 'Подзадача 1', 'UserName1', 'Группа 1', 'Комментарий1', '11.11.2018', '18.11.2018', 'Не выполнено')
    except:
        print("Добавление подзадачи неисправно")

def test_update_under_group_task_in_db():
    try:
        work_with_under_group_tasks.update_under_group_task_in_db(1, 'Подзадача 2', 'UserName1', 'Группа2', 'Комментарий2', '11.11.2018', '18.11.2018', 'Не выполнено', 1)
    except:
        print("Редактирование подзадачи неисправно")

def test_change_status_under_group_task():
    try:
        work_with_under_group_tasks.change_status_under_group_task(1,'Выполнено')
    except:
        print("Редактирование статуса подзадачи неисправно")

def test_delete_group_task_from_db():
    try:
        work_with_group_tasks.change_status_group_task(1, 'Выполнено')
    except:
        print("Удаление групповой задачи неисправно")


if __name__ == '__main__':
    main()