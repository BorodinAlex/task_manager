import ast
import importlib
import datetime


def main():
    '''
    function for interacting with the console
    :return: work with proj
    '''
    command = ''
    while command != 'exit()':
        print('/n Enter the command')
        command = input()
        try:
            f_name, args = parse(command)
            m = __import__('work_package.work_with_consol', fromlist=[None])
            f = getattr(m, f_name)
            f(*args)
        except:
            print('This command does not exist')


def parse(s):
    '''
    parse function request with its arguments
    :param s: function with it's arguments
    :return: function name and arguments
    '''
    last_dot_pos = -1
    first_bracket_pos = s.index('(')
    f_name = s[last_dot_pos+1:first_bracket_pos]
    try:
        args = ast.literal_eval(s[first_bracket_pos+1:-1])
    except:
        args = ""
    return f_name, args


if __name__ == '__main__':
     main()