import psycopg2
import logging
import work_package.work_with_db as work_with_db
import datetime


logging.basicConfig(filename="journal.log", level=logging.INFO)
user_name = ''
user_login = ''


def user_name_is(**kwargs):
    '''
    #########################################
    :param kwargs:
    :return:
    '''
    global user_name
    if len(kwargs) == 0:
        return user_name
    else:
        user_name = str(kwargs.get('logina'))


def check_logs(login):
    '''
    verification of the existence of the login
    :param login: user's login
    :return: TRue or False
    '''
    conn = work_with_db.connect_to_db()
    cursor = conn.cursor()

    cursor.execute("SELECT id,login,passwor FROM users where login=%s", (login, ))
    rows = cursor.fetchall()

    if len(rows) != 0:
        return False
    else:
        return True

    work_with_db.close_cursor(cursor)
    work_with_db.close_connect(conn)


def registration(login, password, question, answer):
    '''
    This function is designed to register new users.
    :param login:  login for registration
    :param password: password to register
    :param question: a secret question for registration, then used to restore a forgotten password
    :param answer: answer to the secret question for registration, then used to restore the forgotten password
    :return: create user in db
    '''
    try:
        if not check_logs(login):
            print('User with this login already exists!')
        else:
            if not check_password(password):
                print('Password must contain at least 8 characters')
            else:
                conn = work_with_db.connect_to_db()
                cursor = conn.cursor()

                cursor.execute("INSERT INTO users (login, passwor, secret_question, answer_on_question) "
                               "VALUES (%s,%s,%s,%s)", (login, password, question, answer))
                conn.commit()

                work_with_db.close_cursor(cursor)
                work_with_db.close_connect(conn)
                print("User {0} was successfully registered!".format(login))
                logging.info(" %s: Successful registration user %s " % (datetime.datetime.now(), login))
    except:
        print("You can not register, please check the connection settings to the database.")
        logging.warning(" %s: Error with registration user %s" % (datetime.datetime.now(), login))


def change_password(question, answer, new_password):
    '''
    this function is for changing the password
    :param question: security question specified during registration
    :param answer: user password for recovery
    :param new_password: new password
    :return: new password
    '''
    try:
        if user_name_is() == '':
            print("You are not logged in. Use the login command ('Your login', 'Your password') to enter the system.")
        else:
            if not check_password(new_password):
                print('Password must contain at least 8 characters')
            else:
                conn = work_with_db.connect_to_db()
                cursor = conn.cursor()

                login = user_name_is()
                cursor.execute("UPDATE users SET passwor = %s WHERE login=%s and secret_question=%s and answer_on_question=%s",
                               (new_password, login, question, answer))
                conn.commit()

                work_with_db.close_cursor(cursor)
                work_with_db.close_connect(conn)
                print('Password for {0} changed successfully!'.format(user_name_is()))
                logging.info(" %s: Successful change password of user %s " % (datetime.datetime.now(), login))
    except:
        print("You can not change the password, please check the connection settings to the database or the correctness of the "
              "sent arguments.")
        logging.warning(" %s: Error with change password of user %s" % (datetime.datetime.now(), user_name_is()))


def reset_password(login, question, answer, new_password):
    '''
    This function is used to restore a forgotten password
    :param login: user's login for recovery
    :param question: security question specified during registration
    :param answer: answer to the secret question specified during registration
    :param new_password: new password
    :return: new password
    '''
    try:
        #####################################
        if not check_password(new_password):
            print('Password must contain at least 8 characters')
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            cursor.execute("UPDATE users SET passwor = %s WHERE login=%s and secret_question=%s and answer_on_question=%s",
                           (new_password, login, question, answer))
            conn.commit()

            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)
            print('Password for {0} successfully restored!'.format(login))
            logging.info(" %s: Successful change reset password of user %s " % (datetime.datetime.now(), login))
    except:
        print("You can not recover the password, please check the connection settings to the database or the correctness of the "
              "sent arguments.")
        logging.warning(" %s: Error with change reset password of user %s" % (datetime.datetime.now(), login))


def check_password(password):
    '''
    password validation
    :param password: user's password
    :return: True or False
    '''
    if len(password) < 8:
        return False
    else:
        return True


def change_status_of_overdue_tasks(row):
    '''
    change the status of the expired task
    :param row: status
    :return: task with new status
    '''
    conn = work_with_db.connect_to_db()
    cursor = conn.cursor()

    for rows in row:
        print(rows[0])
        cursor.execute("UPDATE tasks SET status='Просрочено' WHERE id = %s",
                       (rows[0], ))
        conn.commit()

    work_with_db.close_cursor(cursor)
    work_with_db.close_connect(conn)


def get_id_of_overdue_tasks():
    '''
    gets the id of the expired task
    :return: idre
    '''
    conn = work_with_db.connect_to_db()
    cursor = conn.cursor()

    cursor.execute("SELECT id FROM tasks where date_of_end < %s", (datetime.date.today(),))
    rows = cursor.fetchall()

    change_status_of_overdue_tasks(rows)

    work_with_db.close_cursor(cursor)
    work_with_db.close_connect(conn)


def change_status_of_overdue_group_tasks(row):
    '''
    change the status of the expired group task
    :param row: status ############################
    :return: task with new status
    '''
    conn = work_with_db.connect_to_db()
    cursor = conn.cursor()

    for rows in row:
        print(rows[0])
        cursor.execute("UPDATE group_task SET status='Просрочено' WHERE id = %s",
                       (rows[0], ))
        conn.commit()

    work_with_db.close_cursor(cursor)
    work_with_db.close_connect(conn)


def get_id_of_overdue_group_tasks():
    '''
    gets the id of the expired task
    :return: id
    '''
    conn = work_with_db.connect_to_db()
    cursor = conn.cursor()

    cursor.execute("SELECT id FROM group_task where date_of_end < %s", (datetime.date.today(),))
    rows = cursor.fetchall()

    change_status_of_overdue_group_tasks(rows)

    work_with_db.close_cursor(cursor)
    work_with_db.close_connect(conn)


def change_status_of_overdue_under_group_tasks(row):
    '''
    change the status of the expired subtask
    :param row: status
    :return: task with new status
    '''
    conn = work_with_db.connect_to_db()
    cursor = conn.cursor()

    for rows in row:
        print(rows[0])
        cursor.execute("UPDATE under_group_task SET status='Просрочено' WHERE id = %s",
                       (rows[0], ))
        conn.commit()

    work_with_db.close_cursor(cursor)
    work_with_db.close_connect(conn)


def get_id_of_overdue_under_group_tasks():
    '''
    gets the id of the expired task
    :return: id #######################################
    '''
    conn = work_with_db.connect_to_db()
    cursor = conn.cursor()

    cursor.execute("SELECT id FROM under_group_task where date_of_end < %s", (datetime.date.today(),))
    rows = cursor.fetchall()

    change_status_of_overdue_under_group_tasks(rows)

    work_with_db.close_cursor(cursor)
    work_with_db.close_connect(conn)


def login(login, password):
    '''
       This function is designed to enter the user's private area
       :param login: login specified at registration
       :param password: password specified at registration
       :return: loggined user
       '''
    try:
        conn = work_with_db.connect_to_db()
        cursor = conn.cursor()

        cursor.execute("SELECT id,login,passwor FROM users where login=%s and passwor=%s;", (login, password))
        rows = cursor.fetchall()

        if len(rows) != 0:
            user_name_is(logina=login)
            print('Hi, {0}!'.format(user_name_is()))
        else:
            print('This user does not exist, please try again')
            user_name_is()

        get_id_of_overdue_tasks()
        get_id_of_overdue_group_tasks()
        get_id_of_overdue_under_group_tasks()
        work_with_db.close_cursor(cursor)
        work_with_db.close_connect(conn)

        logging.info(" %s: Successful login user %s " % (datetime.datetime.now(), login))
    except:
        print("You can not log in, please check the database connection settings or the correctness of the "
              "sent arguments.")
        logging.warning(" %s: Error with login user %s" % (datetime.datetime.now(), login))


# def login(login, password):
#     '''
#     This function is designed to enter the user's private area
#     :param login: login specified at registration
#     :param password: password specified at registration
#     :return: loggined user
#     '''
#     try:
#         conn = work_with_db.connect_to_db()
#         cursor = conn.cursor()
#
#         cursor.execute("SELECT id,login,passwor FROM users where login=%s and passwor=%s;", (login, password))
#         rows = cursor.fetchall()
#
#         if len(rows) != 0:
#             user_name_is(logina=login)
#             print('Hi, {0}!'.format(user_name_is()))
#         else:
#             print('This user does not exist, please try again')
#             user_name_is()
#
#         work_with_db.close_cursor(cursor)
#         work_with_db.close_connect(conn)
#
#         logging.info(" %s: Successful login user %s " % (datetime.datetime.now(), login))
#     except:
#         print("You can not log in, please check the database connection settings or the correctness of the "
#               "sent arguments.")
#         logging.warning(" %s: Error with login user %s" % (datetime.datetime.now(), login))


def help_ru():
    print("registration ( login, password, question, answer ) \n"
          "Данная функция предназначена для регистрации новых пользователей.\n"
          "login - логин для регистрации \n"
          "password - пароль для регистрации \n"
          "question - секретный вопрос для регистрации, в дальнейшем используется для восстановления забытого пароля \n"
          "answer - ответ на секретный вопрос для регистрации, в дальнейшем используется для восстановления забытого пароля \n"
          "\n"
          "reset_password ( login, question, answer, new_password ) \n"
          "Данная функция предназначена для восстановления забытого пароля.\n"
          "login - логин пользователя для восстановления \n"
          "password - пароль пользователя для восстановления \n"
          "question - секретный вопрос указанный при регистрации \n"
          "answer - ответ на секретный вопрос указанный при регистрации \n"
          "\n"
          "login ( login, password ) \n"
          "Данная функция предназначена для входа в личный кабинет пользователя\n"
          "login - логин указанный при регистрации \n"
          "password - пароль указанный при регистрации \n"
          "\n"
          "change_password ( question, answer, new_password ) \n"
          "Данная функция предназначена для изменения пароля \n"
          "question - секретный вопрос указанный при регистрации \n"
          "answer - пароль пользователя для восстановления \n"
          "new_password - новый пароль \n"
          "\n"
          "change_password ( question, answer, new_password ) \n"
          "Данная функция предназначена для изменения пароля \n"
          "question - секретный вопрос указанный при регистрации \n"
          "answer - пароль пользователя для восстановления \n"
          "new_password - новый пароль \n"
          "\n"
          "insert_task_into_db(header, tags, comment, date_of_start, date_of_end, status) \n"
          "Данная функция для создания задач \n"
          "header - название задачи\n"
          "tags - тег/группа задачи \n"
          "comment - комментарий к задаче \n"
          "date_of_start - дата начала оковещения \n"
          "date_of_end - дата окончания оповещения\n"
          "status - текущий статус задачи. Может принимать значения 'Выполнено','Не выполнено','В ожидании выполнения' \n"
          "\n"
          "update_task_in_db ( header, tags, comment, date_of_start, date_of_end, status, pk ) \n"
          "Данная функция для редактирования задач \n"
          "header - название задачи\n"
          "tags - тег/группа задачи \n"
          "comment - комментарий к задаче \n"
          "date_of_start - дата начала оковещения \n"
          "date_of_end - дата окончания оповещения\n"
          "status - текущий статус задачи. Может принимать значения 'Выполнено','Не выполнено','В ожидании выполнения' \n"
          "pk - идентификационный ключ задачи(id)\n"
          "\n"
          "delete_task_from_db(pk)\n"
          "Данна функция предназначена для удаления задач\n"
          "pk - идентификационный ключ задачи(id)\n"
          "\n"
          "print_task() \n"
          "Данная функция предназначена для вывода всех задач \n"
          "\n"
          "group_by_for_tasks(column)\n"
          "Данная функция предназначена для группировки задач по определенному полю\n"
          "column - поле, по которому будет происходить группировка. Может принимать значения header, tags, comment, "
          "date_of_start, date_of_end, status, id\n"
          "\n"
          "search_by_header_in_tasks(search_info) || search_by_status_in_tasks(search_info)\n"
          "Данные функции предназначены для поиска информации по Названию (search_by_header_in_tasks) и по Статусу "
          "(search_by_status_in_tasks) задачи\n"
          "search_info - искомое значение поля\n"
          "change_status_task ( pk, status ) \n"
          "Данная функция предназначена для изменения статуса конкретной задачи\n"
          "status - текущий статус задачи \n"
          "pk - идентификационный ключ задачи(id)\n"
          "\n"
          "insert_group_task_into_db ( header, tags, comment, date_of_start, date_of_end, status ) \n"
          "update_group_task_in_db ( header, tags, comment, date_of_start, date_of_end, status, pk ) \n"
          "delete_group_task_from_db ( pk ) \n"
          "print_group_task()\n"
          "group_by_for_group_task ( column ) \n"
          "search_by_status_under_group_task ( search_info ) || search_by_header_under_group_task ( search_info ) \n"
          "change_status_group_task ( pk, status ) \n"
          "\n"
          "insert_under_group_task_into_db ( grou_task_id, header, expert, tags, comment, date_of_start, date_of_end, status )\n"
          "Данная функция предназначена для добавления подзадач для групповых задач\n"
          "grou_task_id - идентификационный ключ задачи(id) для которой создается подзадача\n"
          "update_under_group_task_in_db ( grou_task_id, header, expert, tags, comment, date_of_start, date_of_end, status, id ) \n"
          "delete_under_group_task_from_db(pk)\n"
          "print_under_group_task()\n"
          "group_by_under_group_task(column) - может принимать значения grou_task_id, header, expert, tags, comment, date_of_start, date_of_end, status, id\n"
          "search_by_header_under_group_task || search_by_status_under_group_task\n"
          "change_status_under_group_task(pk, status)\n"
          "print_archive_tasks()\n"
          "Данная функция предназначена для вывода выполенных задач\n"
          "print_archive_group_tasks()\n"
          "Данная функция предназначена для вывода выполенных групповых задач\n"
          "print_archive_under_group_tasks()\n"
          "Данная функция предназначена для вывода выполенных групповых подзадач\n"
           )

def help_en():
    print("registration (login, password, question, answer) \n"
        "This function is designed to register new users. \n"
        "login - login for registration \n"
        "password - password to register \n"
        "question - a secret question for registration, then used to restore a forgotten password \n"
        "answer - answer to the secret question for registration, then used to restore the forgotten password \n"
        "\n"
        "reset_password (login, question, answer, new_password) \n"
        "This function is used to restore a forgotten password. \n"
        "login - user's login for recovery \n"
        "password - user password for recovery \n"
        "question - security question specified during registration \n"
        "answer - answer to the secret question specified during registration \n"
        "\n"
        "login (login, password) \n"
        "This function is designed to enter the user's private area \n"
        "login - login specified at registration \n"
        "password - password specified at registration \n"
        "\n"
        "change_password (question, answer, new_password) \n"
        "This function is for changing the password \n"
        "question - security question specified during registration \n"
        "answer - user password for recovery \n"
        "new_password - new password \n"
        "\n"
        "change_password (question, answer, new_password) \n"
        "This function is for changing the password \n"
        "question - security question specified during registration \n"
        "answer - user password for recovery \n"
        "new_password - new password \n"
        "\n"
        "insert_task_into_db (header, tags, comment, date_of_start, date_of_end, status) \n"
        "This function is for creating tasks \n"
        "header - the name of the task \n"
        "tags - tag / task group \n"
        "comment - comment to the task \n"
        "date_of_start - start date of the window \n"
        "date_of_end - the end date of the notification \n"
        "status is the current status of the task.Can be set to 'Done', 'Not performed', 'Pending execution' \n"
        "\n"
        "update_task_in_db (header, tags, comment, date_of_start, date_of_end, status, pk) \n"
        "This function is for editing tasks \n"
        "header - the name of the task \n"
        "tags - tag / task group \n"
        "comment - comment to the task \n"
        "date_of_start - start date of the window \n"
        "date_of_end - the end date of the notification \n"
        "status is the current status of the task.Can be set to 'Done', 'Not performed', 'Pending execution' \n"
        "pk - task identification key (id) \n"
        "\n"
        "delete_task_from_db (pk) \n"
        "This function is designed to delete tasks \n"
        "pk - task identification key (id) \n"
        "\n"
        "print_task () \n"
        "This function is intended for displaying all tasks \n"
        "\n"
        "group_by_for_tasks (column) \n"
        "This function is intended for grouping tasks on a specific field \n"
        "column is the field by which the grouping will take place.It can take the values ​​of header, tags, comment,"
        "date_of_start, date_of_end, status, id \n"
        "\n"
        "search_by_header_in_tasks (search_info) || search_by_status_in_tasks (search_info) \n"
        "These functions are designed to search for information by name (search_by_header_in_tasks) and by Status"
        "(search_by_status_in_tasks) tasks \n"
        "search_info - the required field value \n"
        "change_status_task (pk, status) \n"
        "This function is intended to change the status of a particular task \n"
        "status - current status of the task \n"
        "pk - task identification key (id) \n"
        "\n"
        "insert_group_task_into_db (header, tags, comment, date_of_start, date_of_end, status) \n"
        "update_group_task_in_db (header, tags, comment, date_of_start, date_of_end, status, pk) \n"
        "delete_group_task_from_db (pk) \n"
        "print_group_task () \n"
        "group_by_for_group_task (column) \n"
        "search_by_status_under_group_task (search_info) || search_by_header_under_group_task (search_info) \n"
        "change_status_group_task (pk, status) \n"
        "\n"
        "insert_under_group_task_into_db (grou_task_id, header, expert, tags, comment, date_of_start, date_of_end, status) \n"
        "This function is for adding subtasks for group tasks \n"
        "grou_task_id - task identification id (id) for which a sub-task is created \n"
        "update_under_group_task_in_db (grou_task_id, header, expert, tags, comment, date_of_start, date_of_end, status, id) \n"
        "delete_under_group_task_from_db (pk) \n"
        "print_under_group_task () \n"
        "group_by_under_group_task (column) - can take the values ​​grou_task_id, header, expert, tags, comment, date_of_start, date_of_end, status, id \n"
        "search_by_header_under_group_task || search_by_status_under_group_task \n"
        "change_status_under_group_task (pk, status) \n"
        "print_archive_tasks () \n"
        "This function is for outputting the completed tasks \n"
        "print_archive_group_tasks () \n"
        "This function is intended for outputting the completed group tasks \n"
        "print_archive_under_group_tasks () \n"
        "This function is designed to output the generated group subtasks \n"
    )