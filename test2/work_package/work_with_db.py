import psycopg2
import logging
import datetime
import json


logging.basicConfig(filename="journal.log", level=logging.INFO)


def cheak_Json_Settings():
    '''
    checks for a Settings.json file
    :return: opens an existing file or creates a new one.
    '''
    try:
        file = open('Settings.json')
    except IOError as e:
        with open('Settings.json', 'w') as outfile:
            json.dump({"Settings_for_dataBase": [{
                            "database": "postgres",
                            "user": "alex",
                            "host": "localhost",
                            "port": "5432",
                            "password": "485285"
                        }
                ]}, outfile)


def get_JSON_Settings():
    '''
    receives data from a Settings.json file
    :return: arguments from Settings.json
    '''
    try:
        cheak_Json_Settings()
        with open('Settings.json', encoding='utf-8') as data_file:
            data = json.loads(data_file.read())
            JSON_database = data['Settings_for_dataBase'][0].get('database')
            JSON_user = data['Settings_for_dataBase'][0].get('user')
            JSON_host = data['Settings_for_dataBase'][0].get('host')
            JSON_port = data['Settings_for_dataBase'][0].get('port')
            JSON_password = data['Settings_for_dataBase'][0].get('password')
        logging.info(" %s: Successful read settings" % (datetime.datetime.now()))
        return JSON_database, JSON_user, JSON_host, JSON_port, JSON_password
    except:
        logging.warning(" %s: Error with reading data from Settings.json" % (datetime.datetime.now()))


def connect_to_db():
    '''
    Creates a connection to the PostgreSQL database
    :return: connection to the database
    '''
    try:
        configs = get_JSON_Settings()
        conn = psycopg2.connect(database=configs[0], user=configs[1], host=configs[2], port=configs[3],
                                password=configs[4])
        logging.info(" %s: Successful connection to the database" % (datetime.datetime.now()))
        return conn
    except:
        logging.warning(" %s: Error with connecting to the database" % (datetime.datetime.now()))


def close_connect(conn):
    '''
    closes the connection to the database
    :param conn: connection to the database
    :return: conn.close
    '''
    try:
        conn.close()
        logging.info(" %s: Successful close connect with database" % (datetime.datetime.now()))
    except:
        logging.warning(" %s: Error with connecting to the database" % (datetime.datetime.now()))


def close_cursor(cursor):
    '''
    removes the cursor from the database
    :param cursor: this is a special object that makes requests and gets their results
    :return: remove cursor
    '''
    try:
        cursor.close()
        logging.info(" %s: Successful close cursor of database" % (datetime.datetime.now()))
    except:
        logging.warning(" %s: Error with close cursor of database" % (datetime.datetime.now()))


def collect_count_of_complete_under_task(pk):
    '''
    returns the number of completed subtasks
    :param pk: personal key of the subtasks
    :return: count of the subtasks/change status of the group tasks
    '''
    try:
        conn = connect_to_db()
        cursor = conn.cursor()

        cursor.execute("SELECT (SELECT grou_task_id FROM under_group_task WHERE id=%s),"
                       "(SELECT COUNT(id) FROM group_task WHERE "
                       "id=(SELECT grou_task_id FROM under_group_task WHERE id=%s)),"
                       "(SELECT COUNT(id) FROM group_task WHERE "
                       "id=(SELECT grou_task_id FROM under_group_task WHERE id=%s and status like 'Выполнено'))"
                       "FROM group_task",
                       (pk, pk, pk, ))
        row = cursor.fetchone()

        close_cursor(cursor)
        close_connect(conn)
        change_status_of_group_task(row)
        logging.info(" %s: Successful collecting count of complete under tasks " % (datetime.datetime.now()))
    except:
        logging.warning(" %s: Error with collecting count of complete under tasks " % (datetime.datetime.now()))


def change_status_of_group_task(row):
    '''
    changes the status of the group task
    :param row: count of the all subtasks and count of the all completed subtasks
    :return: changed status of the group task
    '''
    try:
        conn = connect_to_db()
        cursor = conn.cursor()

        if row[1] == row[2]:
            cursor.execute("UPDATE group_task SET status = 'Выполнено' WHERE id = %s", (int(row[0]), ))
        else:
            cursor.execute("UPDATE group_task SET status = 'В процессе выполнения' WHERE id = %s", (int(row[0]),))
        conn.commit()

        close_cursor(cursor)
        close_connect(conn)
        logging.info(" %s: Successful change status of group task " % (datetime.datetime.now()))
    except:
        logging.warning(" %s: Error with change status of group task " % (datetime.datetime.now()))


def change_childs_tasks(pk):
    '''
    changed status of the subtasks
    :param pk: personal key of the group task
    :return: changed status of the subtasks
    '''
    try:
        conn = connect_to_db()
        cursor = conn.cursor()

        cursor.execute("UPDATE under_group_task SET status = 'Выполнено' WHERE grou_task_id = %s", (pk, ))
        conn.commit()

        close_cursor(cursor)
        close_connect(conn)
        logging.info(" %s: Successful change status of under tasks " % (datetime.datetime.now()))
    except:
        logging.warning(" %s: Error with change status of under tasks " % (datetime.datetime.now()))


def print_list_of_group_task_data(data, cursor):
    '''
    displays a list of all group tasks
    :param data: all parameters of the object - group task
    :param cursor: this is a special object that makes requests and gets their results
    :return: list of all group tasks
    '''
    try:
        datas = data
        while datas is not None:
            print('id: {0} \n'
                  'Group task: {1} \n'
                  'Group: {2} \n'
                  'Comments: {3} \n'
                  'date of creation: {4} \n'
                  'the date of the beginning: {5} \n'
                  'еnd Date: {6} \n'
                  'Status: {7} \n'.format(datas[11], datas[13], datas[14],
                                          datas[15], datas[16], datas[17], datas[18], datas[19]))
        datas = cursor.fetchone()
        logging.info(" %s: Successful print list of group tasks " % (datetime.datetime.now()))
    except:
        logging.warning(" %s: Error with print list of group tasks " % (datetime.datetime.now()))


def print_for_alerts_of_group_task(pk, message):
    '''
    outputs a specific group task after editing it
    :param pk: personal key of the group task
    :param message: group task with a massage
    :return: group task with a massage
    '''
    try:
        conn = connect_to_db()
        cursor = conn.cursor()

        cursor.execute("SELECT * from group_task where id = %s", (pk,))
        row = cursor.fetchone()
        print('Note {0} successfully {1} \n'
              'id: {2} \n'
              'Name: {3} \n'
              'Group: {4} \n'
              'Comments: {5} \n'
              'date of creation: {6} \n'
              'the date of the beginning: {7} \n'
              'еnd Date: {8} \n'
              'Status: {9} \n'.format(row[2], message, row[0], row[2], row[3], row[4], row[5], row[6], row[7], row[8]))

        close_cursor(cursor)
        close_connect(conn)
        logging.info(" %s: Successful print group task " % (datetime.datetime.now()))
    except:
        logging.warning(" %s: Error with print group task " % (datetime.datetime.now()))


def print_list_of_under_group_task_data(data, cursor):
    '''
    displays a list of all subtasks
    :param data: all parameters of the object - subtask
    :param cursor: this is a special object that makes requests and gets their results
    :return: list of all subtasks
    '''
    try:
        datas = data
        while datas is not None:
            print('id: {0} \n'
                  'Group task: {1}(id - {2}) \n'
                  'Subtask name: {3} \n'
                  'Group: {4} \n'
                  'Comments: {5} \n'
                  'date of creation: {6} \n'
                  'the date of the beginning: {7} \n'
                  'еnd Date: {8} \n'
                  'Status: {9} \n'
                  'Executing user: {10} \n'.format(datas[0], datas[13], datas[1], datas[3], datas[4], datas[5], datas[6],
                                                datas[7], datas[8], datas[9], datas[10]))
            datas = cursor.fetchone()
        logging.info(" %s: Successful print list of under group tasks " % (datetime.datetime.now()))
    except:
        logging.warning(" %s: Error with print list of under group tasks " % (datetime.datetime.now()))


def print_for_alert_of_under_task(pk, message):
    '''
    outputs a specific subtask after editing it
    :param pk: personal key of the subtask
    :param message: group task with a massage
    :return: subtask with a massage
    '''
    try:
        conn = connect_to_db()
        cursor = conn.cursor()

        cursor.execute("SELECT * from under_group_task where id = %s", (pk,))
        row = cursor.fetchone()
        print('Note {0} successfully {1} \n'
              'id: {2} \n'
              'Name: {3} \n'
              'Group: {4} \n'
              'Comments: {5} \n'
              'date of creation: {6} \n'
              'the date of the beginning: {7} \n'
              'еnd Date: {8} \n'
              'Status: {9} \n'
              'Executing user: {10} \n'.format(row[3], message, row[0], row[3], row[4], row[5],
                                            row[6], row[7], row[8], row[9], row[10]))

        close_cursor(cursor)
        close_connect(conn)
        logging.info(" %s: Successful print under group tasks " % (datetime.datetime.now()))
    except:
        logging.warning(" %s: Error with print under group tasks " % (datetime.datetime.now()))


def print_list_of_tasks_data(data, cursor):
    '''
    displays a list of all single tasks
    :param data: all parameters of the object - single task
    :param cursor: this is a special object that makes requests and gets their results
    :return: list of all single tasks
    '''
    try:
        datas = data
        while datas is not None:
            print('id: {0} \n'
                  'Task name: {1} \n'
                  'Group: {2} \n'
                  'Comments: {3} \n'
                  'date of creation: {4} \n'
                  'the date of the beginning: {5} \n'
                  'еnd Date: {6} \n'
                  'Status: {7} \n'.format(datas[0], datas[2], datas[3], datas[4], datas[5], datas[6], datas[7], datas[8]))
            datas = cursor.fetchone()
        logging.info(" %s: Successful print list of tasks " % (datetime.datetime.now()))
    except:
        logging.warning(" %s: Error with print list of tasks " % (datetime.datetime.now()))


def print_for_alerts_of_tasks(pk, message):
    '''
    outputs a specific single task after editing it
    :param pk: personal key of the single task
    :param message: single task with a massage
    :return: single task with a massage
    '''
    try:
        conn = connect_to_db()
        cursor = conn.cursor()

        cursor.execute("SELECT * from tasks where id = %s", (pk,))
        row = cursor.fetchone()
        print('Note {0} successfully {1} \n'
              'id: {2} \n'
              'Name: {3} \n'
              'Group: {4} \n'
              'Comments: {5} \n'
              'date of creation: {6} \n'
              'the date of the beginning: {7} \n'
              'еnd Date: {8} \n'
              'Status: {9} \n'.format(row[2], message, row[0], row[2], row[3], row[4], row[5], row[6], row[7], row[8]))

        close_cursor(cursor)
        close_connect(conn)
        logging.info(" %s: Successful print task " % (datetime.datetime.now()))
    except:
        logging.warning(" %s: Error with print task " % (datetime.datetime.now()))


