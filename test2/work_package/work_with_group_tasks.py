import psycopg2
import datetime
import logging
import work_package.work_with_db as work_with_db
import work_package.work_with_users as work_with_users


def insert_group_task_into_db(header, tags, comment, date_of_start, date_of_end, status):
     '''
     adding a new group task to the database
     :param header: the name of the task
     :param tags: tag / task group
     :param comment: comment to the task
     :param date_of_start: the start date of the alert
     :param date_of_end: the end date of the notification
     :param status: is the current status of the task.Can be set to 'Done', 'Not performed', 'Pending execution'
     :return: group task in db
     '''
     try:
         if work_with_users.user_name_is() == '':
             print("You are not logged in. Use the login command ('Your login', 'Your password') to enter the system.")
         else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            if datetime.datetime.strptime(date_of_start, "%d.%m.%Y") > datetime.datetime.strptime(date_of_end, "%d.%m.%Y"):
                print("Start date must not be after the end date")
            else:
                owner = work_with_users.user_name_is()
                cursor.execute("insert into group_task (owner, header, tags, comment, date_of_start, date_of_end, status)"
                               "values(%s,%s,%s,%s,%s,%s,%s)",
                               (owner, header, tags, comment, date_of_start, date_of_end, status))
                conn.commit()
                print('Note {0} successfully added!'.format(header, ))
                logging.info(" %s: Successful insert group task by %s " % (datetime.datetime.now(),
                                                                           work_with_users.user_name_is()))

            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)

     except:
         print("You can not add a Group task, please check the connection settings to the database"
               "or the correctness of the arguments sent.")
         logging.warning(" %s: Error with insert group task by %s" % (datetime.datetime.now(),
                                                                      work_with_users.user_name_is()))


def update_group_task_in_db(header, tags, comment, date_of_start, date_of_end, status, pk):
    '''
    update's group task
    :param header: the name of the task
    :param tags: tag / task group
    :param comment: comment to the task
    :param date_of_start: the start date of the alert
    :param date_of_end: the end date of the notification
    :param status: is the current status of the task.Can be set to 'Done', 'Not performed', 'Pending execution'
    :param pk: personal key of the group task
    :return: updated group task
    '''
    try:
        if work_with_users.user_name_is() == '':
            print("You are not logged in. Use the login command ('Your login', 'Your password') to enter the system.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            if datetime.datetime.strptime(date_of_start, "%d.%m.%Y") > datetime.datetime.strptime(date_of_end, "%d.%m.%Y"):
                print("Start date must not be after the end date")
            else:
                login = work_with_users.user_name_is()
                cursor.execute("UPDATE group_task SET header=%s, tags=%s, comment=%s, "
                               "date_of_start=%s, date_of_end=%s, status=%s WHERE id = %s and owner =%s",
                                (header, tags, comment, date_of_start, date_of_end, status, pk, login))
                conn.commit()
                if status == 'Done':
                    work_with_db.change_childs_tasks(pk)
                work_with_db.print_for_alerts_of_group_task(pk, 'changed')
                logging.info(" %s: Successful update group task by %s " % (datetime.datetime.now(),
                                                                           work_with_users.user_name_is()))

            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)

    except:
        print("You can not update a Group task, please check the connection settings to the database"
              "or the correctness of the arguments sent.")
        logging.warning(" %s: Error with update group task by %s" % (datetime.datetime.now(),
                                                                     work_with_users.user_name_is()))


def delete_group_task_from_db(pk):
    '''
    delete group task from db
    :param pk: personal key of the group task
    :return: delete group task
    '''
    try:
        if work_with_users.user_name_is() == '':
            print("You are not logged in. Use the login command ('Your login', 'Your password') to enter the system.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            cursor.execute("DELETE FROM group_task WHERE id = %s", (pk,))
            conn.commit()

            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)
            work_with_db.print_for_alerts_of_group_task(pk, 'removed')
            logging.info(" %s: Successful delete group task by %s " % (datetime.datetime.now(),
                                                                       work_with_users.user_name_is()))
    except:
        print("You can not delete a Group task, please check the connection settings to the database"
              "or the correctness of the arguments sent.")
        logging.warning(" %s: Error with delete group task by %s" % (datetime.datetime.now(),
                                                                     work_with_users.user_name_is()))


# def print_group_tasks():
#     '''
#     prints group tasks
#     :return: group tasks
#     '''
#     try:
#         if work_with_users.user_name_is() == '':
#             print("You are not logged in. Use the login command ('Your login', 'Your password') to enter the system.")
#         else:
#             conn = work_with_db.connect_to_db()
#             cursor = conn.cursor()
#
#             login = work_with_users.user_name_is()
#             cursor.execute("SELECT * FROM group_task where owner = %s and (status = 'В ожидании выполнения' "
#                            "or status = 'Not done')", (login,))
#             row = cursor.fetchone()
#
#             work_with_db.print_list_of_group_task_data(row, cursor)
#             work_with_db.close_cursor(cursor)
#             work_with_db.close_connect(conn)
#             logging.info(" %s: Successful print group task by %s " % (datetime.datetime.now(),
#                                                                       work_with_users.user_name_is()))
#     except:
#         print("You can not print a Group tasks, please check the connection settings to the database"
#               "or the correctness of the arguments sent.")
#         logging.warning(" %s: Error with print group task by %s" % (datetime.datetime.now(),
#                                                                     work_with_users.user_name_is()))


def print_group_task():
    '''
        prints group tasks
        :return: group tasks
        '''
    try:
        if work_with_users.user_name_is() == '':
            print("You are not logged in. Use the login command ('Your login', 'Your password') to enter the system.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            login = work_with_users.user_name_is()
            cursor.execute("SELECT * FROM group_task where owner = %s and (status='В ожидании выполнения' "
                           "or status='Pending execution')", (login,))
            row = cursor.fetchone()

            work_with_db.print_list_of_group_task_data(row, cursor)
            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)
            logging.info(" %s: Successful print group task by %s " % (datetime.datetime.now(),
                                                                      work_with_users.user_name_is()))
    except:
        print("You can not print a Group tasks, please check the connection settings to the database"
              "or the correctness of the arguments sent.")
        logging.warning(" %s: Error with print group task by %s" % (datetime.datetime.now(),
                                                                    work_with_users.user_name_is()))


def print_archive_tasks():
    '''
       returns group tasks from the archive
       :return: group tasks from the archive
       '''
    try:
        if work_with_users.user_name_is() == '':
            print("You are not logged in. Use the login command ('Your login', 'Your password') to enter the system.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            login = work_with_users.user_name_is()
            cursor.execute("SELECT * FROM group_task where owner = %s and (status='Done' or status='overdue')", (login,))
            row = cursor.fetchone()

            work_with_db.print_list_of_group_task_data(row, cursor)
            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)
            logging.info(" %s: Successful print archived group task by %s " % (datetime.datetime.now(),
                                                                      work_with_users.user_name_is()))
    except:
        print("You can not print archive Group task, please check the connection settings to the database"
              "or the correctness of the arguments sent.")
        logging.warning(" %s: Error with print archived group task by %s" % (datetime.datetime.now(),
                                                                    work_with_users.user_name_is()))


# def print_archive_tasks():
#     '''
#     returns group tasks from the archive
#     :return: group tasks from the archive
#     '''
#     try:
#         if work_with_users.user_name_is() == '':
#             print("You are not logged in. Use the login command ('Your login', 'Your password') to enter the system.")
#         else:
#             conn = work_with_db.connect_to_db()
#             cursor = conn.cursor()
#
#             login = work_with_users.user_name_is()
#             cursor.execute("SELECT * FROM group_task where owner = %s and (status = 'Done')", (login,))
#             row = cursor.fetchone()
#
#             work_with_db.print_list_of_group_task_data(row, cursor)
#             work_with_db.close_cursor(cursor)
#             work_with_db.close_connect(conn)
#             logging.info(" %s: Successful print archived group task by %s " % (datetime.datetime.now(),
#                                                                       work_with_users.user_name_is()))
#     except:
#         print("You can not print archive Group task, please check the connection settings to the database"
#               "or the correctness of the arguments sent.")
#         logging.warning(" %s: Error with print archived group task by %s" % (datetime.datetime.now(),
#                                                                     work_with_users.user_name_is()))



def group_by_for_group_task(column):
    '''
    group task grouping
    :param column: the parameter by which the grouping takes place
    :return: grouped group tasks
    '''
    try:
        if work_with_users.user_name_is() == '':
            print("You are not logged in. Use the login command ('Your login', 'Your password') to enter the system.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            login = work_with_users.user_name_is()
            cursor.execute("SELECT * from under_group_task, group_task where "
                           "(under_group_task.owner=%s or under_group_task.expert = %s) and "
                           "(under_group_task.grou_task_id = group_task.id) ORDER BY group_task."+column+"", (login, login, ))
            row = cursor.fetchone()
            work_with_db.print_list_of_group_task_data(row, cursor)

            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)
            logging.info(" %s: Successful grouped group task by %s " % (datetime.datetime.now(),
                                                                        work_with_users.user_name_is()))
    except:
        print("You can not group a Group tasks, please check the connection settings to the database"
              "or the correctness of the arguments sent.")
        logging.warning(" %s: Error with grouped group task by %s" % (datetime.datetime.now(),
                                                                      work_with_users.user_name_is()))


def search_by_header_group_task(search_info):
    '''
    search by name
    :param search_info: the parameter by which the search takes place
    :return: search results
    '''
    try:
        if work_with_users.user_name_is() == '':
            print("You are not logged in. Use the login command ('Your login', 'Your password') to enter the system.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            login = work_with_users.user_name_is()
            cursor.execute("SELECT * from under_group_task, group_task WHERE "
                           "(under_group_task.header like %s) and (under_group_task.grou_task_id = group_task.id) "
                           "and (under_group_task.owner=%s or under_group_task.expert = %s)",
                           (search_info, login, login))
            row = cursor.fetchone()

            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)
            work_with_db.print_list_of_group_task_data(row, cursor)
            logging.info(" %s: Successful search in group task by %s " % (datetime.datetime.now(),
                                                                          work_with_users.user_name_is()))
    except:
        print("You can not perform a search, please check the connection settings to the database or "
              "the correctness of the arguments sent.")
        logging.warning(" %s: Error with search in group task by %s" % (datetime.datetime.now(),
                                                                        work_with_users.user_name_is()))


def search_by_status_group_task(search_info):
    '''
    search by status
    :param search_info: the parameter by which the search takes place
    :return: search results
    '''
    try:
        if work_with_users.user_name_is() == '':
            print("You are not logged in. Use the login command ('Your login', 'Your password') to enter the system.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            login = work_with_users.user_name_is()
            cursor.execute("SELECT * from under_group_task, group_task WHERE (under_group_task.status like %s) "
                           "and (under_group_task.grou_task_id = group_task.id) and "
                           "(under_group_task.owner=%s or under_group_task.expert = %s) ",
                           (search_info, login, login))
            row = cursor.fetchone()

            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)
            work_with_db.print_list_of_group_task_data(row, cursor)
            logging.info(" %s: Successful search in group task by %s " % (datetime.datetime.now(),
                                                                          work_with_users.user_name_is()))
    except:
        print("You can not perform a search, please check the connection settings to the database or "
              "the correctness of the arguments sent.")
        logging.warning(" %s: Error with search in group task by %s" % (datetime.datetime.now(),
                                                                        work_with_users.user_name_is()))


def change_status_group_task(pk, status):
    '''
    changes the status of the group task
    :param pk: group task personal key
    :param status: the status to which we change
    :return: changed group task status
    '''
    try:
        if work_with_users.user_name_is() == '':
            print("You are not logged in. Use the login command ('Your login', 'Your password') to enter the system.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            login = work_with_users.user_name_is()
            cursor.execute("UPDATE group_task SET status = %s WHERE id = %s and owner = %s ", (status, pk, login,))
            conn.commit()

            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)

            if status == 'Done':
                work_with_db.change_childs_tasks(pk)
            work_with_db.print_for_alerts_of_group_task(pk, 'changed')
            logging.info(" %s: Successful change status of group task by %s " % (datetime.datetime.now(),
                                                                                 work_with_users.user_name_is()))
    except:
        print("You can not change the status of the task, please check the connection settings to "
              "the database or the correctness of the arguments sent.")
        logging.warning(" %s: Error with change status of group task by %s" % (datetime.datetime.now(),
                                                                               work_with_users.user_name_is()))
