import psycopg2
import work_package.work_with_db as work_with_db
import work_package.work_with_users as work_with_users
import datetime
import logging


def insert_under_group_task_into_db(grou_task_id, header, expert, tags, comment, date_of_start, date_of_end, status):
    '''

    :param grou_task_id:
    :param header: name of the subtask
    :param expert: task executor
    :param tags: tag / task group
    :param comment: comment to the task
    :param date_of_start: the start date of the alert
    :param date_of_end: the end date of the notification
    :param status: is the current status of the task.Can be set to 'Done', 'Not performed', 'Pending execution'
    :return: subtask in db
    '''
    try:
        if work_with_users.user_name_is() == '':
            print("You are not logged in. Use the login command ('Your login', 'Your password') to enter the system.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            if datetime.datetime.strptime(date_of_start, "%d.%m.%Y") > datetime.datetime.strptime(date_of_end, "%d.%m.%Y"):
                print("Start date must not be after the end date")
            else:
                owner = work_with_users.user_name_is()
                cursor.execute("insert into under_group_task (grou_task_id, owner, header, tags, comment, date_of_start,"
                               " date_of_end, "
                               "status, expert) values(%s,%s,%s,%s,%s,%s,%s,%s,%s)",
                               (grou_task_id, owner, header, tags, comment, date_of_start,
                                date_of_end, status, expert))
                conn.commit()
                print('Note {0} successfully added!'.format(header, ))
                logging.info(" %s: Successful insert under group task by %s " % (datetime.datetime.now(),
                                                                                 work_with_users.user_name_is()))

            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)
    except:
        print("You can not add a subtask, please check the connection settings to the database"
              "or the correctness of the arguments sent.")
        logging.warning(" %s: Error with insert under group task by %s" % (datetime.datetime.now(),
                                                                           work_with_users.user_name_is()))


def update_under_group_task_in_db(grou_task_id, header, expert, tags, comment, date_of_start, date_of_end, status, id):
    '''
    update's sub task
    :param grou_task_id:
    :param expert: task executor
    :param header: the name of the task
    :param tags: tag / task group
    :param comment: comment to the task
    :param date_of_start: the start date of the alert
    :param date_of_end: the end date of the notification
    :param status: is the current status of the task.Can be set to 'Done', 'Not performed', 'Pending execution'
    :param id: id of the subtask
    :return: updated subtask
    '''
    try:
        if work_with_users.user_name_is() == '':
            print("You are not logged in. Use the login command ('Your login', 'Your password') to enter the system.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            if datetime.datetime.strptime(date_of_start, "%d.%m.%Y") > datetime.datetime.strptime(date_of_end, "%d.%m.%Y"):
                print("Start date must not be after the end date")
            else:
                login = work_with_users.user_name_is()
                cursor.execute("UPDATE under_group_task SET grou_task_id=%s, header=%s, tags=%s, "
                               "comment=%s, date_of_start=%s, date_of_end=%s, status=%s, expert =%s "
                               "WHERE id = %s and owner = %s",
                               (grou_task_id, header, tags, comment, date_of_start, date_of_end, status, expert, id, login))
                conn.commit()
                if status == 'Done':
                    work_with_db.collect_count_of_complete_under_task(grou_task_id)
                work_with_db.print_for_alerts_of_group_task(id, 'changed')
                logging.info(" %s: Successful update under group task by %s " % (datetime.datetime.now(),
                                                                                 work_with_users.user_name_is()))
            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)

    except:
        print("You can not change the Group Subtask, please check the connection settings for the "
              "or the correctness of the arguments sent.")
        logging.warning(" %s: Error with update under group task by %s" % (datetime.datetime.now(),
                                                                           work_with_users.user_name_is()))


def delete_under_group_task_from_db(pk):
    '''
    delete subtask from db
    :param pk: personal key of the subtask
    :return: delete subtask
    '''
    try:
        if work_with_users.user_name_is() == '':
            print("You are not logged in. Use the login command ('Your login', 'Your password') to enter the system.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            login = work_with_users.user_name_is()
            cursor.execute("DELETE FROM under_group_task WHERE id = %s and owner = %s", (pk, login))
            conn.commit()

            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)
            print("The node with id = {0} was successfully deleted".format(pk))
            logging.info(" %s: Successful delete under group task by %s " % (datetime.datetime.now(),
                                                                             work_with_users.user_name_is()))

    except:
        print("You can not delete the Group Subtask, please check the connection settings for the "
              "or the correctness of the arguments sent.")
        logging.warning(" %s: Error with delete under group task by %s" % (datetime.datetime.now(),
                                                                           work_with_users.user_name_is()))


# def print_under_group_task():
#     '''
#     prints subtasks
#     :return: subtasks
#     '''
#     try:
#         if work_with_users.user_name_is() == '':
#             print("You are not logged in. Use the login command ('Your login', 'Your password') to enter the system.")
#         else:
#             conn = work_with_db.connect_to_db()
#             cursor = conn.cursor()
#
#             login = work_with_users.user_name_is()
#             cursor.execute("SELECT * FROM under_group_task where (owner = %s or expert =%) and "
#                            "(status = 'Pending execution' or status = 'Not done')", (login, login,))
#             row = cursor.fetchone()
#
#             work_with_db.print_list_of_under_group_task_data(row, cursor)
#             work_with_db.close_cursor(cursor)
#             work_with_db.close_connect(conn)
#             logging.info(" %s: Successful print under group task by %s " % (datetime.datetime.now(),
#                                                                             work_with_users.user_name_is()))
#     except:
#         print("You can not view the list of Group Subtasks, please check the connection settings for "
#               "the database or the correctness of the arguments sent")
#         logging.warning(" %s: Error with print under group task by %s" % (datetime.datetime.now(),
#                                                                           work_with_users.user_name_is()))

def print_under_group_task():
    '''
     prints subtasks
     :return: subtasks
     '''
    try:
        if work_with_users.user_name_is() == '':
            print("You are not logged in. Use the login command ('Your login', 'Your password') to enter the system.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            login = work_with_users.user_name_is()
            cursor.execute("SELECT * FROM under_group_task where (owner = %s or expert =%) and "
                           "(status='В ожидании выполнения' or status='Pending execution')", (login, login,))
            row = cursor.fetchone()

            work_with_db.print_list_of_under_group_task_data(row, cursor)
            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)
            logging.info(" %s: Successful print under group task by %s " % (datetime.datetime.now(),
                                                                            work_with_users.user_name_is()))
    except:
        print("You can not view the list of Group Subtasks, please check the connection settings for "
              "the database or the correctness of the arguments sent")
        logging.warning(" %s: Error with print under group task by %s" % (datetime.datetime.now(),
                                                                          work_with_users.user_name_is()))


def print_arhive_under_group_task():
    '''
    returns subtasks from the archive
    :return: subtasks from the archive
    '''
    try:
        if work_with_users.user_name_is() == '':
            print("You are not logged in. Use the login command ('Your login', 'Your password') to enter the system.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            login = work_with_users.user_name_is()
            cursor.execute("SELECT * FROM under_group_task where (owner = %s or expert =%) and "
                           "(status='Done' or status='overdue')", (login, login,))
            row = cursor.fetchone()

            work_with_db.print_list_of_under_group_task_data(row, cursor)
            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)
            logging.info(" %s: Successful print archived under group task by %s " % (datetime.datetime.now(),
                                                                            work_with_users.user_name_is()))
    except:
        print("You can not view the list of completed Group Subtasks, please check the connection settings for "
              "the database or the correctness of the arguments sent.")
        logging.warning(" %s: Error with print archived under group task by %s" % (datetime.datetime.now(),
                                                                          work_with_users.user_name_is()))



# def print_arhive_under_group_task():
#     '''
#     returns subtasks from the archive
#     :return: subtasks from the archive
#     '''
#     try:
#         if work_with_users.user_name_is() == '':
#             print("You are not logged in. Use the login command ('Your login', 'Your password') to enter the system.")
#         else:
#             conn = work_with_db.connect_to_db()
#             cursor = conn.cursor()
#
#             login = work_with_users.user_name_is()
#             cursor.execute("SELECT * FROM under_group_task where (owner = %s or expert =%) and "
#                            "(status = 'Done')", (login, login,))
#             row = cursor.fetchone()
#
#             work_with_db.print_list_of_under_group_task_data(row, cursor)
#             work_with_db.close_cursor(cursor)
#             work_with_db.close_connect(conn)
#             logging.info(" %s: Successful print archived under group task by %s " % (datetime.datetime.now(),
#                                                                             work_with_users.user_name_is()))
#     except:
#         print("You can not view the list of completed Group Subtasks, please check the connection settings for "
#               "the database or the correctness of the arguments sent.")
#         logging.warning(" %s: Error with print archived under group task by %s" % (datetime.datetime.now(),
#                                                                           work_with_users.user_name_is()))



def group_by_under_group_task(column):
    '''
    subtask grouping
    :param column: the parameter by which the grouping takes place
    :return: grouped subtasks
    '''
    try:
        if work_with_users.user_name_is() == '':
            print("You are not logged in. Use the login command ('Your login', 'Your password') to enter the system.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            login = work_with_users.user_name_is()
            cursor.execute("Select * from under_group_task, group_task where (under_group_task.owner =%s "
                           "or under_group_task.expert=%s) and (under_group_task.grou_task_id = group_task.id) "
                           "ORDER BY under_group_task."+column+"", (login, login))
            row = cursor.fetchone()
            work_with_db.print_list_of_under_group_task_data(row, cursor)

            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)
            logging.info(" %s: Successful grouped under group tasks by %s " % (datetime.datetime.now(),
                                                                               work_with_users.user_name_is()))

    except:
        print("You can not add grouped tasks, please check the connection settings for the database "
              "or the correctness of the arguments sent.")
        logging.warning(" %s: Error with grouped under group tasks by %s" % (datetime.datetime.now(),
                                                                             work_with_users.user_name_is()))


def search_by_header_under_group_task(search_info):
    '''
        search by name
        :param search_info: the parameter by which the search takes place
        :return: search results
        '''
    try:
        if work_with_users.user_name_is() == '':
            print("You are not logged in. Use the login command ('Your login', 'Your password') to enter the system.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            login = work_with_users.user_name_is()
            cursor.execute("SELECT * from under_group_task, group_task WHERE "
                           "(under_group_task.header like %s) and (under_group_task.grou_task_id = group_task.id) "
                           "and (under_group_task.owner=%s or under_group_task.expert = %s)",
                           (search_info, login, login))
            row = cursor.fetchone()

            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)
            logging.info(" %s: Successful searched under group task by %s " % (datetime.datetime.now(),
                                                                               work_with_users.user_name_is()))

    except:
        print("You can not perform a search, please check the connection settings to the database or "
              "the correctness of the arguments sent.")
        logging.warning(" %s: Error with searched under  group task by %s" % (datetime.datetime.now(),
                                                                              work_with_users.user_name_is()))


def search_by_status_under_group_task(search_info):
    '''
        search by status
        :param search_info: the parameter by which the search takes place
        :return: search results
        '''
    try:
        if work_with_users.user_name_is() == '':
            print("You are not logged in. Use the login command ('Your login', 'Your password') to enter the system.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            login = work_with_users.user_name_is()
            cursor.execute("SELECT * from under_group_task, group_task WHERE (under_group_task.status like %s) "
                           "and (under_group_task.grou_task_id = group_task.id) "
                           "and (under_group_task.owner=%s or under_group_task.expert = %s) ",
                           (search_info, login, login))
            row = cursor.fetchone()

            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)
            logging.info(" %s: Successful searched under group task by %s " % (datetime.datetime.now(),
                                                                               work_with_users.user_name_is()))

    except:
        print("You can not perform a search, please check the connection settings to the database or "
              "the correctness of the arguments sent.")
        logging.warning(" %s: Error with searched under group task by %s" % (datetime.datetime.now(),
                                                                             work_with_users.user_name_is()))


def change_status_under_group_task(pk, status):
    '''
           changes the status of the subtask
           :param pk: subtask personal key
           :param status: the status to which we change
           :return: changed subtask status
           '''
    try:
        if work_with_users.user_name_is() == '':
            print("You are not logged in. Use the login command ('Your login', 'Your password') to enter the system.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            login = work_with_users.user_name_is()
            cursor.execute("UPDATE under_group_task SET status = %s WHERE id = %s and owner = %s ",
                           (status, pk, login, ))
            conn.commit()

            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)

            if status == 'Done':
                work_with_db.collect_count_of_complete_under_task(pk)
            work_with_db.print_for_alerts_of_group_task(pk, 'changed')

            logging.info(" %s: Successful changed under group task by %s " % (datetime.datetime.now(),
                                                                              work_with_users.user_name_is()))

    except:
        print("You can not change the status of the task, please check the connection settings to "
              "the database or the correctness of the arguments sent.")
        logging.warning(" %s: Error with changed under group task by %s" % (datetime.datetime.now(),
                                                                            work_with_users.user_name_is()))