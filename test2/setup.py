from distutils.core import setup
from setuptools import find_packages

setup(name='work_package',
      version='0.1',
      description='My first package',
      author='Alex',
      license='Settings.json',
      author_email='your@email.com',
      packages=find_packages(),
      zip_safe=False,
      include_package_data=True,
      entry_points={
              'console_scripts':
                  ['task_mg = work_package.console:main']
          }, install_requires=['click']
      )
