import psycopg2
import datetime
import logging
import work_package.work_with_db as work_with_db
import work_package.work_with_users as work_with_users


def insert_group_task_into_db(header, tags, comment, date_of_start, date_of_end, status):
     try:
         if work_with_users.user_name_is() == '':
             print("Вы не вошли в систему. Воспользуйтесь командой login('Ваш логин','Ваш пароль') для входа в сисему.")
         else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            if datetime.datetime.strptime(date_of_start, "%d.%m.%Y") > datetime.datetime.strptime(date_of_end, "%d.%m.%Y"):
                print("Дата начала не должна быть позже даты окончания")
            else:
                owner = work_with_users.user_name_is()
                cursor.execute("insert into group_task (owner, header, tags, comment, date_of_start, date_of_end, status)"
                               "values(%s,%s,%s,%s,%s,%s,%s)",
                               (owner, header, tags, comment, date_of_start, date_of_end, status))
                conn.commit()
                print('Запись {0} успешно добавлена!'.format(header, ))
                logging.info(" %s: Successful insert group task by %s " % (datetime.datetime.now(),
                                                                           work_with_users.user_name_is()))

            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)

     except:
         print("Вы не можете добавить Групповую задачу, пожалуйста проверьте настройки подключения к базе данных "
               "или правильность посылаемых аргументов.")
         logging.warning(" %s: Error with insert group task by %s" % (datetime.datetime.now(),
                                                                  work_with_users.user_name_is()))


def update_group_task_in_db(header, tags, comment, date_of_start, date_of_end, status, pk):
    try:
        if work_with_users.user_name_is() == '':
            print("Вы не вошли в систему. Воспользуйтесь командой login('Ваш логин','Ваш пароль') для входа в сисему.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            if datetime.datetime.strptime(date_of_start, "%d.%m.%Y") > datetime.datetime.strptime(date_of_end, "%d.%m.%Y"):
                print("Дата начала не должна быть позже даты окончания")
            else:
                login = work_with_users.user_name_is()
                cursor.execute("UPDATE group_task SET header=%s, tags=%s, comment=%s, "
                               "date_of_start=%s, date_of_end=%s, status=%s WHERE id = %s and owner =%s",
                                (header, tags, comment, date_of_start, date_of_end, status, pk, login))
                conn.commit()
                if status == 'Выполнено':
                    work_with_db.change_childs_tasks(pk)
                work_with_db.print_for_alerts_of_group_task(pk, 'изменена')
                logging.info(" %s: Successful update group task by %s " % (datetime.datetime.now(),
                                                                           work_with_users.user_name_is()))

            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)

    except:
        print("Вы не можете изменить Групповую задачу, пожалуйста проверьте настройки подключения к базе "
              "данных или правильность посылаемых аргументов.")
        logging.warning(" %s: Error with update group task by %s" % (datetime.datetime.now(),
                                                                     work_with_users.user_name_is()))


def delete_group_task_from_db(pk):
    try:
        if work_with_users.user_name_is() == '':
            print("Вы не вошли в систему. Воспользуйтесь командой login('Ваш логин','Ваш пароль') для входа в сисему.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            cursor.execute("DELETE FROM group_task WHERE id = %s", (pk,))
            conn.commit()

            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)
            work_with_db.print_for_alerts_of_group_task(pk, 'удалена')
            logging.info(" %s: Successful delete group task by %s " % (datetime.datetime.now(),
                                                                       work_with_users.user_name_is()))
    except:
        print("Вы не можете удалить Групповую задачу, пожалуйста проверьте настройки подключения к базе "
              "данных или правильность посылаемых аргументов.")
        logging.warning(" %s: Error with delete group task by %s" % (datetime.datetime.now(),
                                                                     work_with_users.user_name_is()))


def print_group_task():
    try:
        if work_with_users.user_name_is() == '':
            print("Вы не вошли в систему. Воспользуйтесь командой login('Ваш логин','Ваш пароль') для входа в сисему.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            login = work_with_users.user_name_is()
            cursor.execute("SELECT * FROM group_task where owner = %s and (status = 'В ожидании выполнения' "
                           "or status = 'Не выполнено')", (login,))
            row = cursor.fetchone()

            work_with_db.print_list_of_group_task_data(row, cursor)
            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)
            logging.info(" %s: Successful print group task by %s " % (datetime.datetime.now(),
                                                                      work_with_users.user_name_is()))
    except:
        print("Вы не можете просмотреть список Групповых задач, пожалуйста проверьте настройки подключения к "
              "базе данных или правильность посылаемых аргументов.")
        logging.warning(" %s: Error with print group task by %s" % (datetime.datetime.now(),
                                                                    work_with_users.user_name_is()))


def print_archive_tasks():
    try:
        if work_with_users.user_name_is() == '':
            print("Вы не вошли в систему. Воспользуйтесь командой login('Ваш логин','Ваш пароль') для входа в сисему.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            login = work_with_users.user_name_is()
            cursor.execute("SELECT * FROM group_task where owner = %s and (status = 'Выполнено')", (login,))
            row = cursor.fetchone()

            work_with_db.print_list_of_group_task_data(row, cursor)
            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)
            logging.info(" %s: Successful print archived group task by %s " % (datetime.datetime.now(),
                                                                      work_with_users.user_name_is()))
    except:
        print("Вы не можете просмотреть список выполненых групповых задач, пожалуйста проверьте настройки подключения к "
              "базе данных или правильность посылаемых аргументов.")
        logging.warning(" %s: Error with print archived group task by %s" % (datetime.datetime.now(),
                                                                    work_with_users.user_name_is()))



def group_by_for_group_task(column):
    try:
        if work_with_users.user_name_is() == '':
            print("Вы не вошли в систему. Воспользуйтесь командой login('Ваш логин','Ваш пароль') для входа в сисему.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            login = work_with_users.user_name_is()
            cursor.execute("SELECT * from under_group_task, group_task where "
                           "(under_group_task.owner=%s or under_group_task.expert = %s) and "
                           "(under_group_task.grou_task_id = group_task.id) ORDER BY group_task."+column+"", (login, login, ))
            row = cursor.fetchone()
            work_with_db.print_list_of_group_task_data(row, cursor)

            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)
            logging.info(" %s: Successful grouped group task by %s " % (datetime.datetime.now(),
                                                                        work_with_users.user_name_is()))
    except:
        print("Вы не можете просмотреть Сгруппированный список, пожалуйста проверьте настройки подключения к "
              "базе данных или правильность посылаемых аргументов.")
        logging.warning(" %s: Error with grouped group task by %s" % (datetime.datetime.now(),
                                                                      work_with_users.user_name_is()))


def search_by_header_group_task(search_info):
    try:
        if work_with_users.user_name_is() == '':
            print("Вы не вошли в систему. Воспользуйтесь командой login('Ваш логин','Ваш пароль') для входа в сисему.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            login = work_with_users.user_name_is()
            cursor.execute("SELECT * from under_group_task, group_task WHERE "
                           "(under_group_task.header like %s) and (under_group_task.grou_task_id = group_task.id) "
                           "and (under_group_task.owner=%s or under_group_task.expert = %s)",
                           (search_info, login, login))
            row = cursor.fetchone()

            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)
            work_with_db.print_list_of_group_task_data(row, cursor)
            logging.info(" %s: Successful search in group task by %s " % (datetime.datetime.now(),
                                                                          work_with_users.user_name_is()))
    except:
        print("Вы не можете произвести поиск, пожалуйста проверьте настройки подключения к базе данных или правильность"
              "посылаемых аргументов.")
        logging.warning(" %s: Error with search in group task by %s" % (datetime.datetime.now(),
                                                                        work_with_users.user_name_is()))


def search_by_status_group_task(search_info):
    try:
        if work_with_users.user_name_is() == '':
            print("Вы не вошли в систему. Воспользуйтесь командой login('Ваш логин','Ваш пароль') для входа в сисему.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            login = work_with_users.user_name_is()
            cursor.execute("SELECT * from under_group_task, group_task WHERE (under_group_task.status like %s) "
                           "and (under_group_task.grou_task_id = group_task.id) and "
                           "(under_group_task.owner=%s or under_group_task.expert = %s) ",
                           (search_info, login, login))
            row = cursor.fetchone()

            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)
            work_with_db.print_list_of_group_task_data(row, cursor)
            logging.info(" %s: Successful search in group task by %s " % (datetime.datetime.now(),
                                                                          work_with_users.user_name_is()))
    except:
        print("Вы не можете произвести поиск, пожалуйста проверьте настройки подключения к базе данных или правильность"
              "посылаемых аргументов.")
        logging.warning(" %s: Error with search in group task by %s" % (datetime.datetime.now(),
                                                                        work_with_users.user_name_is()))


def change_status_group_task(pk, status):
    try:
        if work_with_users.user_name_is() == '':
            print("Вы не вошли в систему. Воспользуйтесь командой login('Ваш логин','Ваш пароль') для входа в сисему.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            login = work_with_users.user_name_is()
            cursor.execute("UPDATE group_task SET status = %s WHERE id = %s and owner = %s ", (status, pk, login,))
            conn.commit()

            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)

            if status == 'Выполнено':
                work_with_db.change_childs_tasks(pk)
            work_with_db.print_for_alerts_of_group_task(pk, 'изменена')
            logging.info(" %s: Successful change status of group task by %s " % (datetime.datetime.now(),
                                                                                 work_with_users.user_name_is()))
    except:
        print("Вы не можете изменить статус задачи, пожалуйста проверьте настройки подключения к базе "
              "данных или правильность посылаемых аргументов.")
        logging.warning(" %s: Error with change status of group task by %s" % (datetime.datetime.now(),
                                                                               work_with_users.user_name_is()))
