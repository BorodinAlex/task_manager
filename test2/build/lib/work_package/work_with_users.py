import psycopg2
import logging
import work_package.work_with_db as work_with_db
import datetime


logging.basicConfig(filename="journal.log", level=logging.INFO)
user_name = ''
user_login = ''


def user_name_is(**kwargs):
    global user_name
    if len(kwargs) == 0:
        return user_name
    else:
        user_name = str(kwargs.get('logina'))


def check_logs(login):
    conn = work_with_db.connect_to_db()
    cursor = conn.cursor()

    cursor.execute("SELECT id,login,passwor FROM users where login=%s", (login, ))
    rows = cursor.fetchall()

    if len(rows) != 0:
        return False
    else:
        return True

    work_with_db.close_cursor(cursor)
    work_with_db.close_connect(conn)


def registration(login, password, question, answer):
    try:
        if not check_logs(login):
            print('Пользователь с данным логином уже существует!')
        else:
            if not check_password(password):
                print('Пароль должен содержать не менее 8 символов')
            else:
                conn = work_with_db.connect_to_db()
                cursor = conn.cursor()

                cursor.execute("INSERT INTO users (login, passwor, secret_question, answer_on_question) "
                               "VALUES (%s,%s,%s,%s)", (login, password, question, answer))
                conn.commit()

                work_with_db.close_cursor(cursor)
                work_with_db.close_connect(conn)
                print("Пользователь {0} был успешно зарегестрирован!".format(login))
                logging.info(" %s: Successful registration user %s " % (datetime.datetime.now(), login))
    except:
        print("Вы не можете зарегистрировать, пожалуйста проверьте настройки подключения к базе данных.")
        logging.warning(" %s: Error with registration user %s" % (datetime.datetime.now(), login))


def change_password(question, answer, new_password):
    try:
        if user_name_is() == '':
            print("Вы не вошли в систему. Воспользуйтесь командой login('Ваш логин','Ваш пароль') для входа в сисему.")
        else:
            if not check_password(new_password):
                print('Пароль должен содержать не менее 8 символов')
            else:
                conn = work_with_db.connect_to_db()
                cursor = conn.cursor()

                login = user_name_is()
                cursor.execute("UPDATE users SET passwor = %s WHERE login=%s and secret_question=%s and answer_on_question=%s",
                               (new_password, login, question, answer))
                conn.commit()

                work_with_db.close_cursor(cursor)
                work_with_db.close_connect(conn)
                print('Пароль для {0} успешно изменен!'.format(user_name_is()))
                logging.info(" %s: Successful change password of user %s " % (datetime.datetime.now(), login))
    except:
        print("Вы не можете изменить пароль, пожалуйста проверьте настройки подключения к базе данных или правильность "
              "посылаемых аргументов.")
        logging.warning(" %s: Error with change password of user %s" % (datetime.datetime.now(), user_name_is()))


def reset_password(login, question, answer, new_password):
    try:
        if not check_password(new_password):
            print('Пароль должен содержать не менее 8 символов')
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            cursor.execute("UPDATE users SET passwor = %s WHERE login=%s and secret_question=%s and answer_on_question=%s",
                           (new_password, login, question, answer))
            conn.commit()

            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)
            print('Пароль для {0} успешно восстановлен!'.format(login))
            logging.info(" %s: Successful change reset password of user %s " % (datetime.datetime.now(), login))
    except:
        print("Вы не можете восстановить пароль, пожалуйста проверьте настройки подключения к базе данных или правильность "
              "посылаемых аргументов.")
        logging.warning(" %s: Error with change reset password of user %s" % (datetime.datetime.now(), login))


def check_password(password):
    if len(password) < 8:
        return False
    else:
        return True


def login(login, password):
    try:
        conn = work_with_db.connect_to_db()
        cursor = conn.cursor()

        cursor.execute("SELECT id,login,passwor FROM users where login=%s and passwor=%s;", (login, password))
        rows = cursor.fetchall()

        if len(rows) != 0:
            user_name_is(logina=login)
            print('Здравствуй, {0}!'.format(user_name_is()))
        else:
            print('Данного пользователя не существует, повторите попытку')
            user_name_is()

        work_with_db.close_cursor(cursor)
        work_with_db.close_connect(conn)

        logging.info(" %s: Successful login user %s " % (datetime.datetime.now(), login))
    except:
        print("Вы не можете войти, пожалуйста проверьте настройки подключения к базе данных или правильность "
              "посылаемых аргументов.")
        logging.warning(" %s: Error with login user %s" % (datetime.datetime.now(), login))


def help():
    print("registration ( login, password, question, answer ) \n"
          "Данная функция предназначена для регистрации новых пользователей.\n"
          "login - логин для регистрации \n"
          "password - пароль для регистрации \n"
          "question - секретный вопрос для регистрации, в дальнейшем используется для восстановления забытого пароля \n"
          "answer - ответ на секретный вопрос для регистрации, в дальнейшем используется для восстановления забытого пароля \n"
          "\n"
          "reset_password ( login, question, answer, new_password ) \n"
          "Данная функция предназначена для восстановления забытого пароля.\n"
          "login - логин пользователя для восстановления \n"
          "password - пароль пользователя для восстановления \n"
          "question - секретный вопрос указанный при регистрации \n"
          "answer - ответ на секретный вопрос указанный при регистрации \n"
          "\n"
          "login ( login, password ) \n"
          "Данная функция предназначена для входа в личный кабинет пользователя\n"
          "login - логин указанный при регистрации \n"
          "password - пароль указанный при регистрации \n"
          "\n"
          "change_password ( question, answer, new_password ) \n"
          "Данная функция предназначена для изменения пароля \n"
          "question - секретный вопрос указанный при регистрации \n"
          "answer - пароль пользователя для восстановления \n"
          "new_password - новый пароль \n"
          "\n"
          "change_password ( question, answer, new_password ) \n"
          "Данная функция предназначена для изменения пароля \n"
          "question - секретный вопрос указанный при регистрации \n"
          "answer - пароль пользователя для восстановления \n"
          "new_password - новый пароль \n"
          "\n"
          "insert_task_into_db(header, tags, comment, date_of_start, date_of_end, status) \n"
          "Данная функция для создания задач \n"
          "header - название задачи\n"
          "tags - тег/группа задачи \n"
          "comment - комментарий к задаче \n"
          "date_of_start - дата начала оковещения \n"
          "date_of_end - дата окончания оповещения\n"
          "status - текущий статус задачи. Может принимать значения 'Выполнено','Не выполнено','В ожидании выполнения' \n"
          "\n"
          "update_task_in_db ( header, tags, comment, date_of_start, date_of_end, status, pk ) \n"
          "Данная функция для редактирования задач \n"
          "header - название задачи\n"
          "tags - тег/группа задачи \n"
          "comment - комментарий к задаче \n"
          "date_of_start - дата начала оковещения \n"
          "date_of_end - дата окончания оповещения\n"
          "status - текущий статус задачи. Может принимать значения 'Выполнено','Не выполнено','В ожидании выполнения' \n"
          "pk - идентификационный ключ задачи(id)\n"
          "\n"
          "delete_task_from_db(pk)\n"
          "Данна функция предназначена для удаления задач\n"
          "pk - идентификационный ключ задачи(id)\n"
          "\n"
          "print_task() \n"
          "Данная функция предназначена для вывода всех задач \n"
          "\n"
          "group_by_for_tasks(column)\n"
          "Данная функция предназначена для группировки задач по определенному полю\n"
          "column - поле, по которому будет происходить группировка. Может принимать значения header, tags, comment, "
          "date_of_start, date_of_end, status, id\n"
          "\n"
          "search_by_header_in_tasks(search_info) || search_by_status_in_tasks(search_info)\n"
          "Данные функции предназначены для поиска информации по Названию (search_by_header_in_tasks) и по Статусу "
          "(search_by_status_in_tasks) задачи\n"
          "search_info - искомое значение поля\n"
          "change_status_task ( pk, status ) \n"
          "Данная функция предназначена для изменения статуса конкретной задачи\n"
          "status - текущий статус задачи \n"
          "pk - идентификационный ключ задачи(id)\n"
          "\n"
          "insert_group_task_into_db ( header, tags, comment, date_of_start, date_of_end, status ) \n"
          "update_group_task_in_db ( header, tags, comment, date_of_start, date_of_end, status, pk ) \n"
          "delete_group_task_from_db ( pk ) \n"
          "print_group_task()\n"
          "group_by_for_group_task ( column ) \n"
          "search_by_status_under_group_task ( search_info ) || search_by_header_under_group_task ( search_info ) \n"
          "change_status_group_task ( pk, status ) \n"
          "\n"
          "insert_under_group_task_into_db ( grou_task_id, header, expert, tags, comment, date_of_start, date_of_end, status )\n"
          "Данная функция предназначена для добавления подзадач для групповых задач\n"
          "grou_task_id - идентификационный ключ задачи(id) для которой создается подзадача\n"
          "update_under_group_task_in_db ( grou_task_id, header, expert, tags, comment, date_of_start, date_of_end, status, id ) \n"
          "delete_under_group_task_from_db(pk)\n"
          "print_under_group_task()\n"
          "group_by_under_group_task(column) - может принимать значения grou_task_id, header, expert, tags, comment, date_of_start, date_of_end, status, id\n"
          "search_by_header_under_group_task || search_by_status_under_group_task\n"
          "change_status_under_group_task(pk, status)\n"
          "print_archive_tasks()\n"
          "Данная функция предназначена для вывода выполенных задач\n"
          "print_archive_group_tasks()\n"
          "Данная функция предназначена для вывода выполенных групповых задач\n"
          "print_archive_under_group_tasks()\n"
          "Данная функция предназначена для вывода выполенных групповых подзадач\n"
           )