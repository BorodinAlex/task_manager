import psycopg2
import logging
import datetime
import work_package.work_with_db as work_with_db
import work_package.work_with_users as work_with_users
logging.basicConfig(filename="journal.log", level=logging.INFO)


def insert_task_into_db(header, tags, comment, date_of_start, date_of_end, status):
    try:
        if work_with_users.user_name_is() == '':
            print("Вы не вошли в систему. Воспользуйтесь командой login('Ваш логин','Ваш пароль') для входа в сисему.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            if datetime.datetime.strptime(date_of_start, "%d.%m.%Y") > datetime.datetime.strptime(date_of_end, "%d.%m.%Y"):
                print("Дата начала не должна быть позже даты окончания")
            else:
                owner = work_with_users.user_name_is()
                cursor.execute("insert into tasks (owner, header, tags, comment, date_of_start, date_of_end, status) "
                               "values(%s,%s,%s,%s,%s,%s,%s)", (owner, header, tags, comment,
                                                                date_of_start, date_of_end, status))
                conn.commit()
                print('Запись {0} успешно добавлена!'.format(header, ))
                logging.info(" %s: Successful insert task by %s " % (datetime.datetime.now(),
                                                                     work_with_users.user_name_is()))

            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)
    except:
        print("Вы не можете добавить Задачу, пожалуйста проверьте настройки подключения к базе данных или правильность"
              "посылаемых аргументов.")
        logging.warning(" %s: Error with insert task by %s" % (datetime.datetime.now(), work_with_users.user_name_is()))


def update_task_in_db(header, tags, comment, date_of_start, date_of_end, status, pk):
    try:
        if work_with_users.user_name_is() == '':
            print("Вы не вошли в систему. Воспользуйтесь командой login('Ваш логин','Ваш пароль') для входа в сисему.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            if datetime.datetime.strptime(date_of_start, "%d.%m.%Y") > datetime.datetime.strptime(date_of_end, "%d.%m.%Y"):
                print("Дата начала не должна быть позже даты окончания")
            else:
                login = work_with_users.user_name_is()
                cursor.execute("UPDATE tasks SET header=%s, tags=%s, comment=%s, date_of_start=%s, date_of_end=%s, status=%s"
                               "WHERE id = %s and owner = %s", (header, tags, comment, date_of_start,
                                                                date_of_end, status, pk, login))
                conn.commit()
                work_with_db.print_for_alerts_of_tasks(pk, 'изменена')
                logging.info(" %s: Successful update task by %s " % (datetime.datetime.now(),
                                                                     work_with_users.user_name_is()))

            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)
    except:
        print("Вы не можете изменить Задачу, пожалуйста проверьте настройки подключения к базе данных или правильность "
              "посылаемых аргументов.")
        logging.warning(" %s: Error with update task by %s" % (datetime.datetime.now(),
                                                               work_with_users.user_name_is()))


def delete_task_from_db(pk):
    try:
        if work_with_users.user_name_is() == '':
            print("Вы не вошли в систему. Воспользуйтесь командой login('Ваш логин','Ваш пароль') для входа в сисему.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            login = work_with_users.user_name_is()
            cursor.execute("DELETE FROM tasks WHERE id = %s and owner=%s", (pk, login))
            conn.commit()

            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)
            print("Зпипись с id = {0} была успешно удалена".format(pk))
            logging.info(" %s: Successful delete task by %s " % (datetime.datetime.now(),
                                                                 work_with_users.user_name_is()))
    except:
        print("Вы не можете удалить Задачу, пожалуйста проверьте настройки подключения к базе данных или правильность "
              "посылаемых аргументов.")
        logging.warning(" %s: Error with delete task by %s" % (datetime.datetime.now(),
                                                               work_with_users.user_name_is()()))


def print_task():
    try:
        if work_with_users.user_name_is() == '':
            print("Вы не вошли в систему. Воспользуйтесь командой login('Ваш логин','Ваш пароль') для входа в сисему.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            login = work_with_users.user_name_is()
            cursor.execute("SELECT * FROM tasks where owner = %s and (status='Не выполнено' "
                           "or status='В процессе выполнения')", (login, ))
            row = cursor.fetchone()

            work_with_db.print_list_of_tasks_data(row, cursor)
            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)
            logging.info(" %s: Successful print task for %s " % (datetime.datetime.now(),
                                                                 work_with_users.user_name_is()))
    except:
        print("Вы не можете просмотреть список Задач, пожалуйста проверьте настройки подключения к базе данных "
              "или правильность посылаемых аргументов.")
        logging.warning(" %s: Error with print task for %s" % (datetime.datetime.now(),
                                                               work_with_users.user_name_is()))


def print_archived_task():
    try:
        if work_with_users.user_name_is() == '':
            print("Вы не вошли в систему. Воспользуйтесь командой login('Ваш логин','Ваш пароль') для входа в сисему.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            login = work_with_users.user_name_is()
            cursor.execute("SELECT * FROM tasks where owner = %s and (status='Выполнено')", (login, ))
            row = cursor.fetchone()

            work_with_db.print_list_of_tasks_data(row, cursor)
            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)
            logging.info(" %s: Successful print  task for %s " % (datetime.datetime.now(),
                                                                 work_with_users.user_name_is()))
    except:
        print("Вы не можете просмотреть список выполненых Задач, пожалуйста проверьте настройки подключения к базе данных "
              "или правильность посылаемых аргументов.")
        logging.warning(" %s: Error with print task for %s" % (datetime.datetime.now(),
                                                               work_with_users.user_name_is()))



def group_by_for_tasks(column):
    try:
        if work_with_users.user_name_is() == '':
            print("Вы не вошли в систему. Воспользуйтесь командой login('Ваш логин','Ваш пароль') для входа в сисему.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            login = work_with_users.user_name_is()
            cursor.execute("SELECT * from tasks where owner =%s ORDER BY "+column+"", (login, ))
            row = cursor.fetchone()

            work_with_db.print_list_of_tasks_data(row, cursor)
            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)
            logging.info(" %s: Successful grouped task by %s " % (datetime.datetime.now(),
                                                                  work_with_users.user_name_is()))
    except:
        print("Вы не можете просмотреть отгруппированный список Задач, пожалуйста проверьте настройки подключения"
              " к базе данных или правильность посылаемых аргументов.")
        logging.warning(" %s: Error with grouped task by %s" % (datetime.datetime.now(),
                                                                work_with_users.user_name_is()))


def search_by_header_in_tasks(search_info):
    try:
        if work_with_users.user_name_is() == '':
            print("Вы не вошли в систему. Воспользуйтесь командой login('Ваш логин','Ваш пароль') для входа в сисему.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            login = work_with_users.user_name_is()
            cursor.execute("SELECT * from tasks WHERE header like %s and owner = %s",
                           (search_info, login))
            row = cursor.fetchone()

            work_with_db.print_list_of_tasks_data(row, cursor)
            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)
            logging.info(" %s: Successful search task by %s " % (datetime.datetime.now(),
                                                                 work_with_users.user_name_is()))
    except:
        print("Вы не можете произвести поиск, пожалуйста проверьте настройки подключения к базе данных "
              "или правильность посылаемых аргументов.")
        logging.warning(" %s: Error with search task by %s" % (datetime.datetime.now(),
                                                               work_with_users.user_name_is()))


def search_by_status_in_tasks(search_info):
    try:
        if work_with_users.user_name_is() == '':
            print("Вы не вошли в систему. Воспользуйтесь командой login('Ваш логин','Ваш пароль') для входа в сисему.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            login = work_with_users.user_name_is()
            cursor.execute("SELECT * from tasks WHERE status like %s and owner = %s",
                           (search_info, login))
            row = cursor.fetchone()

            work_with_db.print_list_of_tasks_data(row, cursor)
            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)
            logging.info(" %s: Successful search task by %s " % (datetime.datetime.now(),
                                                                 work_with_users.user_name_is()))
    except:
        print("Вы не можете произвести поиск, пожалуйста проверьте настройки подключения к базе данных "
              "или правильность посылаемых аргументов.")
        logging.warning(" %s: Error with search task by %s" % (datetime.datetime.now(),
                                                               work_with_users.user_name_is()))


def change_status_task(pk, status):
    try:
        if work_with_users.user_name_is() == '':
            print("Вы не вошли в систему. Воспользуйтесь командой login('Ваш логин','Ваш пароль') для входа в сисему.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            login = work_with_users.user_name_is()
            cursor.execute("UPDATE tasks SET status = %s WHERE id = %s and owner = %s ", (status, pk, login, ))
            conn.commit()

            work_with_db.print_for_alerts_of_tasks(pk, 'изменена')
            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)
            logging.info(" %s: Successful change status task by %s " % (datetime.datetime.now(),
                                                                                    work_with_users.user_name_is()))
    except:
        print("Вы не можете изменить пароль, пожалуйста проверьте настройки подключения к базе данных или правильность "
              "посылаемых аргументов.")
        logging.warning(" %s: Error with change status task by %s" % (datetime.datetime.now(),
                                                                                  work_with_users.user_name_is()))
