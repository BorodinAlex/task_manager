import psycopg2
import work_package.work_with_db as work_with_db
import work_package.work_with_users as work_with_users
import datetime
import logging


def insert_under_group_task_into_db(grou_task_id, header, expert, tags, comment, date_of_start, date_of_end, status):
    try:
        if work_with_users.user_name_is() == '':
            print("Вы не вошли в систему. Воспользуйтесь командой login('Ваш логин','Ваш пароль') для входа в сисему.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            if datetime.datetime.strptime(date_of_start, "%d.%m.%Y") > datetime.datetime.strptime(date_of_end, "%d.%m.%Y"):
                print("Дата начала не должна быть позже даты окончания")
            else:
                owner = work_with_users.user_name_is()
                cursor.execute("insert into under_group_task (grou_task_id, owner, header, tags, comment, date_of_start,"
                               " date_of_end, "
                               "status, expert) values(%s,%s,%s,%s,%s,%s,%s,%s,%s)",
                               (grou_task_id, owner, header, tags, comment, date_of_start,
                                date_of_end, status, expert))
                conn.commit()
                print('Запись {0} успешно добавлена!'.format(header, ))
                logging.info(" %s: Successful insert under group task by %s " % (datetime.datetime.now(),
                                                                                 work_with_users.user_name_is()))

            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)
    except:
        print("Вы не можете добавить Групповую подзадачу, пожалуйста проверьте настройки подключения к базе данных "
              "или правильность посылаемых аргументов.")
        logging.warning(" %s: Error with insert under group task by %s" % (datetime.datetime.now(),
                                                                           work_with_users.user_name_is()))


def update_under_group_task_in_db(grou_task_id, header, expert, tags, comment, date_of_start, date_of_end, status, id):
    try:
        if work_with_users.user_name_is() == '':
            print("Вы не вошли в систему. Воспользуйтесь командой login('Ваш логин','Ваш пароль') для входа в сисему.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            if datetime.datetime.strptime(date_of_start, "%d.%m.%Y") > datetime.datetime.strptime(date_of_end, "%d.%m.%Y"):
                print("Дата начала не должна быть позже даты окончания")
            else:
                login = work_with_users.user_name_is()
                cursor.execute("UPDATE under_group_task SET grou_task_id=%s, header=%s, tags=%s, "
                               "comment=%s, date_of_start=%s, date_of_end=%s, status=%s, expert =%s "
                               "WHERE id = %s and owner = %s",
                               (grou_task_id, header, tags, comment, date_of_start, date_of_end, status, expert, id, login))
                conn.commit()
                if status == 'Выполнена':
                    work_with_db.collect_count_of_complete_under_task(grou_task_id)
                work_with_db.print_for_alerts_of_group_task(id, 'изменена')
                logging.info(" %s: Successful update under group task by %s " % (datetime.datetime.now(),
                                                                                 work_with_users.user_name_is()))
            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)

    except:
        print("Вы не можете изменить Групповую Подзадачу, пожалуйста проверьте настройки подключения к базе данных "
              "или правильность посылаемых аргументов.")
        logging.warning(" %s: Error with update under group task by %s" % (datetime.datetime.now(),
                                                                           work_with_users.user_name_is()))


def delete_under_group_task_from_db(pk):
    try:
        if work_with_users.user_name_is() == '':
            print("Вы не вошли в систему. Воспользуйтесь командой login('Ваш логин','Ваш пароль') для входа в сисему.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            login = work_with_users.user_name_is()
            cursor.execute("DELETE FROM under_group_task WHERE id = %s and owner = %s", (pk, login))
            conn.commit()

            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)
            print("Запись с id = {0} была успешно удалена".format(pk))
            logging.info(" %s: Successful delete under group task by %s " % (datetime.datetime.now(),
                                                                             work_with_users.user_name_is()))

    except:
        print("Вы не можете удалить Групповую Подзадачу, пожалуйста проверьте настройки подключения к базе данных "
              "или правильность посылаемых аргументов.")
        logging.warning(" %s: Error with delete under group task by %s" % (datetime.datetime.now(),
                                                                           work_with_users.user_name_is()))


def print_under_group_task():
    try:
        if work_with_users.user_name_is() == '':
            print("Вы не вошли в систему. Воспользуйтесь командой login('Ваш логин','Ваш пароль') для входа в сисему.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            login = work_with_users.user_name_is()
            cursor.execute("SELECT * FROM under_group_task where (owner = %s or expert =%) and "
                           "(status = 'В ожидании выполнения' or status = 'Не выполнено')", (login, login,))
            row = cursor.fetchone()

            work_with_db.print_list_of_under_group_task_data(row, cursor)
            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)
            logging.info(" %s: Successful print under group task by %s " % (datetime.datetime.now(),
                                                                            work_with_users.user_name_is()))
    except:
        print("Вы не можете просмотреть список Групповых Подзадач, пожалуйста проверьте настройки подключения к "
              "базе данных или правильность посылаемых аргументов.")
        logging.warning(" %s: Error with print under group task by %s" % (datetime.datetime.now(),
                                                                          work_with_users.user_name_is()))


def print_arhive_under_group_task():
    try:
        if work_with_users.user_name_is() == '':
            print("Вы не вошли в систему. Воспользуйтесь командой login('Ваш логин','Ваш пароль') для входа в сисему.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            login = work_with_users.user_name_is()
            cursor.execute("SELECT * FROM under_group_task where (owner = %s or expert =%) and "
                           "(status = 'Выполнено')", (login, login,))
            row = cursor.fetchone()

            work_with_db.print_list_of_under_group_task_data(row, cursor)
            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)
            logging.info(" %s: Successful print archived under group task by %s " % (datetime.datetime.now(),
                                                                            work_with_users.user_name_is()))
    except:
        print("Вы не можете просмотреть список выполненных Групповых Подзадач, пожалуйста проверьте настройки подключения к "
              "базе данных или правильность посылаемых аргументов.")
        logging.warning(" %s: Error with print archived under group task by %s" % (datetime.datetime.now(),
                                                                          work_with_users.user_name_is()))



def group_by_under_group_task(column):
    try:
        if work_with_users.user_name_is() == '':
            print("Вы не вошли в систему. Воспользуйтесь командой login('Ваш логин','Ваш пароль') для входа в сисему.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            login = work_with_users.user_name_is()
            cursor.execute("Select * from under_group_task, group_task where (under_group_task.owner =%s "
                           "or under_group_task.expert=%s) and (under_group_task.grou_task_id = group_task.id) "
                           "ORDER BY under_group_task."+column+"", (login, login))
            row = cursor.fetchone()
            work_with_db.print_list_of_under_group_task_data(row, cursor)

            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)
            logging.info(" %s: Successful grouped under group tasks by %s " % (datetime.datetime.now(),
                                                                               work_with_users.user_name_is()))

    except:
        print("Вы не можете добавить сгруппированные задачи, пожалуйста проверьте настройки подключения к базе данных "
              "или правильность посылаемых аргументов.")
        logging.warning(" %s: Error with grouped under group tasks by %s" % (datetime.datetime.now(),
                                                                             work_with_users.user_name_is()))


def search_by_header_under_group_task(search_info):
    try:
        if work_with_users.user_name_is() == '':
            print("Вы не вошли в систему. Воспользуйтесь командой login('Ваш логин','Ваш пароль') для входа в сисему.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            login = work_with_users.user_name_is()
            cursor.execute("SELECT * from under_group_task, group_task WHERE "
                           "(under_group_task.header like %s) and (under_group_task.grou_task_id = group_task.id) "
                           "and (under_group_task.owner=%s or under_group_task.expert = %s)",
                           (search_info, login, login))
            row = cursor.fetchone()

            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)
            logging.info(" %s: Successful searched under group task by %s " % (datetime.datetime.now(),
                                                                               work_with_users.user_name_is()))

    except:
        print("Вы не можете произвести поиск, пожалуйста проверьте настройки подключения к базе данных "
              "или правильность посылаемых аргументов.")
        logging.warning(" %s: Error with searched under  group task by %s" % (datetime.datetime.now(),
                                                                              work_with_users.user_name_is()))


def search_by_status_under_group_task(search_info):
    try:
        if work_with_users.user_name_is() == '':
            print("Вы не вошли в систему. Воспользуйтесь командой login('Ваш логин','Ваш пароль') для входа в сисему.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            login = work_with_users.user_name_is()
            cursor.execute("SELECT * from under_group_task, group_task WHERE (under_group_task.status like %s) "
                           "and (under_group_task.grou_task_id = group_task.id) "
                           "and (under_group_task.owner=%s or under_group_task.expert = %s) ",
                           (search_info, login, login))
            row = cursor.fetchone()

            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)
            logging.info(" %s: Successful searched under group task by %s " % (datetime.datetime.now(),
                                                                               work_with_users.user_name_is()))

    except:
        print("Вы не можете произвести поиск, пожалуйста проверьте настройки подключения к базе данных "
              "или правильность посылаемых аргументов.")
        logging.warning(" %s: Error with searched under group task by %s" % (datetime.datetime.now(),
                                                                             work_with_users.user_name_is()))


def change_status_under_group_task(pk, status):
    try:
        if work_with_users.user_name_is() == '':
            print("Вы не вошли в систему. Воспользуйтесь командой login('Ваш логин','Ваш пароль') для входа в сисему.")
        else:
            conn = work_with_db.connect_to_db()
            cursor = conn.cursor()

            login = work_with_users.user_name_is()
            cursor.execute("UPDATE under_group_task SET status = %s WHERE id = %s and owner = %s ",
                           (status, pk, login, ))
            conn.commit()

            work_with_db.close_cursor(cursor)
            work_with_db.close_connect(conn)

            if status == 'Выполнено':
                work_with_db.collect_count_of_complete_under_task(pk)
            work_with_db.print_for_alerts_of_group_task(pk, 'изменена')

            logging.info(" %s: Successful changed under group task by %s " % (datetime.datetime.now(),
                                                                              work_with_users.user_name_is()))

    except:
        print("Вы не можете изменить статус задачи, пожалуйста проверьте настройки подключения к базе данных "
              "или правильность посылаемых аргументов.")
        logging.warning(" %s: Error with changed under group task by %s" % (datetime.datetime.now(),
                                                                            work_with_users.user_name_is()))