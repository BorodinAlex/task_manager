import work_package.work_with_users as work_with_users
import work_package.work_with_db as work_with_db
import work_package.work_with_under_group_tasks as work_with_under_group_tasks
import work_package.work_with_group_tasks as work_with_group_tasks
import work_package.work_with_task as work_with_task
import work_package.work_with_tests as work_with_tests
import ast


def login(login, password):
    work_with_users.login(login,password)

def reset_password(login, question, answer, new_password):
    work_with_users.reset_password(login, question, answer, new_password)

def change_password(question, answer, new_password):
    work_with_users.change_password(question, answer, new_password)

def registration(login, password, question, answer):
    work_with_users.registration(login, password, question, answer)

def help():
    work_with_users.help()



def insert_task_into_db(header, tags, comment, date_of_start, date_of_end, status):
    work_with_task.insert_task_into_db(header, tags, comment, date_of_start, date_of_end, status)

def update_task_in_db(header, tags, comment, date_of_start, date_of_end, status, pk):
    work_with_task.update_task_in_db(header, tags, comment, date_of_start, date_of_end, status, pk)

def delete_task_from_db(pk):
    work_with_task.delete_task_from_db(pk)

def print_task():
    work_with_task.print_task()

def print_archived_task():
    work_with_task.print_archived_task()

def group_by_for_tasks(column):
    work_with_task.group_by_for_tasks(column)

def search_by_header_in_tasks(search_info):
    work_with_task.search_by_header_in_tasks(search_info)

def search_by_status_in_tasks(search_info):
    work_with_task.search_by_status_in_tasks(search_info)

def change_status_task(pk, status):
    work_with_task.change_status_task(pk, status)



def insert_group_task_into_db(header, tags, comment, date_of_start, date_of_end, status):
    work_with_group_tasks.insert_group_task_into_db(header, tags, comment, date_of_start, date_of_end, status)

def update_group_task_in_db(header, tags, comment, date_of_start, date_of_end, status, pk):
    work_with_group_tasks.update_group_task_in_db(header, tags, comment, date_of_start, date_of_end, status, pk)

def delete_group_task_from_db(pk):
    work_with_group_tasks.delete_group_task_from_db(pk)

def print_group_task():
    work_with_group_tasks.print_group_task()

def print_archive_tasks():
    work_with_group_tasks.print_archive_tasks()

def group_by_for_group_task(column):
    work_with_group_tasks.group_by_for_group_task(column)

def search_by_header_group_task(search_info):
    work_with_group_tasks.search_by_header_group_task(search_info)

def search_by_status_group_task(search_info):
    work_with_group_tasks.search_by_status_group_task(search_info)

def change_status_group_task(pk, status):
    work_with_group_tasks.change_status_group_task(pk, status)



def insert_under_group_task_into_db(grou_task_id, header, expert, tags, comment, date_of_start, date_of_end, status):
    work_with_under_group_tasks.insert_under_group_task_into_db(grou_task_id, header, expert, tags, comment, date_of_start, date_of_end, status)

def update_under_group_task_in_db(grou_task_id, header, expert, tags, comment, date_of_start, date_of_end, status, id):
    work_with_under_group_tasks.update_under_group_task_in_db(grou_task_id, header, expert, tags, comment, date_of_start, date_of_end, status, id)

def delete_under_group_task_from_db(pk):
    work_with_under_group_tasks.delete_under_group_task_from_db(pk)

def print_under_group_task():
    work_with_under_group_tasks.print_under_group_task()

def print_arhive_under_group_task():
    work_with_under_group_tasks.print_arhive_under_group_task()

def group_by_under_group_task(column):
    work_with_under_group_tasks.group_by_under_group_task(column)

def search_by_header_under_group_task(search_info):
    work_with_under_group_tasks.search_by_header_under_group_task(search_info)

def search_by_status_under_group_task(search_info):
    work_with_under_group_tasks.search_by_status_under_group_task(search_info)

def change_status_under_group_task(pk, status):
    work_with_under_group_tasks.change_status_under_group_task(pk, status)


def test():
    work_with_tests.main()