####Bralex library
##About content

Bralex library is a library written in Python, which is a task tracker application that can help you manage your personal time or, if you are a team leader, set tasks correctly and share time within the team. You can use this library both to develop your console application and to develop a complete web application based on this library.

##Library content features

    User registration system.
    Add, edit, delete a project. 
    Add executors to a project
    Create, delete, edit a category in tasks
    Create, delete, and modify tasks
    Showing all tasks of user
    Ability to create subtasks
    Ability to create assosiation between to tasks

##The contents of the repository


Content:

    Bralex library

How to install?

    Swap to your virtual enviroment
    Write the following command in terminal

    $ python setup.py install


##The use of the application

To test the application, write

    $ bralex_mg

to the console. This is a keyword that you can use to print to the console to interact with the library. The console parser provides communication with the library and thus forms a single application. 

##Running tests

    run in the console Bralex
    Write

    $ test()

##Project Author

    Borodin Alexandr
    Group 653502

