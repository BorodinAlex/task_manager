import unittest
from datetime import datetime
# from sqlalchemy.exc import *
#
# from pack.methods.task_methods import *
from pack.methods.user_methods import *


class TestsTask(unittest.TestCase):

    def test_task_add_good(self):
        login = "test_user"
        password = "new_test_password"
        header = 'Стул'
        priority = 'main'
        tags = 'Мебель'
        comment = 'деревяный стол'
        date_of_start = "12-09-2017"
        time_of_start = "16:40"
        date_of_end = "15-09-2017"
        time_of_end = "13:21"
        status = 'Done'


        try:
            TaskAddMethods.add_task(login, password, header, priority, tags, comment, date_of_start, time_of_start,
                                    date_of_end, time_of_end, status)
            status = True
        except:
            status = False
            self.assertEqual(status, True)

    def test_task_add_date_wrong(self):
        login = "test_user"
        password = "new_test_password"
        header = 'Стол'
        priority = 'main'
        tags = 'Мебель'
        comment = 'деревяный стол'
        date_of_start = "15-09-2017"
        time_of_start = "16:40"
        date_of_end = "12-09-2017"
        time_of_end = "13:21"
        status = 'Done'

        try:
            TaskAddMethods.add_task(login, password, header, priority, tags, comment, date_of_start, time_of_start,
                                    date_of_end, time_of_end, status)
            status = True
            # delete
        except:
            status = False

        self.assertEqual(status, True)

    def test_task_add_date_no_user(self):
        login = "test_user"
        password = "new_test_password"
        header = 'Стол'
        priority = 'main'
        tags = 'Мебель'
        comment = 'деревяный стол'
        date_of_start = "12-09-2017"
        time_of_start = "16:40"
        date_of_end = "15-09-2017"
        time_of_end = "13:21"
        status = 'Done'

        try:
            TaskAddMethods.add_task(login, password, header, priority, tags, comment, date_of_start, time_of_start,
                                    date_of_end, time_of_end, status)
            status = True
            #delete
        except:
            status = False
            self.assertEqual(status, True)

    def test_task_delete_good(self):
        login = "test_user"
        password = "new_test_password"
        task_id = '14'

        try:
            TaskDeleteMethods.delete_task(login, password, task_id)
            status = True
            #delete
        except:
            status = False
            self.assertEqual(status, True)

    def test_task_delete_no_owner(self):
        login = "aa"
        password = "new_test_password"
        task_id = '10'

        try:
            TaskDeleteMethods.delete_task(login, password, task_id)
            status = True
            #delete
        except:
            status = False
            self.assertEqual(status, True)

    def test_task_delete_no_user(self):
        login = "aabbb"
        password = "new_test_password"
        task_id = '10'

        try:
            TaskDeleteMethods.delete_task(login, password, task_id)
            status = True
            #delete
        except:
            status = False
            self.assertEqual(status, True)

    def test_task_delete_no_task_id(self):
        login = "aa"
        password = "new_test_password"
        task_id = '21'

        try:
            TaskDeleteMethods.delete_task(login, password, task_id)
            status = True
            #delete
        except:
            status = False
            self.assertEqual(status, True)


class TestsChangeTask(unittest.TestCase):

    def test_task_change_date_good(self):
        login = "test_user"
        password = "new_test_password"
        task_id = '9'
        date_of_start = "12-05-2017"
        time_of_start = "20:40"
        date_of_end = "15-11-2017"
        time_of_end = "13:21"

        try:
            TaskChangeMethods.change_task_date(login, password, task_id, date_of_start, date_of_end, time_of_start,
                                               time_of_end)
            status = True
            #delete
        except:
            status = False
            self.assertEqual(status, True)

    def test_task_change_date_no_owner(self):
        login = "testf_user"
        password = "new_test_password"
        task_id = '9'
        date_of_start = "12-05-2017"
        time_of_start = "20:40"
        date_of_end = "15-11-2017"
        time_of_end = "13:21"

        try:
            TaskChangeMethods.change_task_date(login, password, task_id, date_of_start, date_of_end, time_of_start,
                                               time_of_end)
            status = True
            #delete
        except:
            status = False
            self.assertEqual(status, True)

    def test_task_change_date_no_owners(self):
        login = "test_user"
        password = "new_test_password"
        task_id = '1'
        date_of_start = "17-11-2017"
        time_of_start = "20:40"
        date_of_end = "15-11-2017"
        time_of_end = "13:21"

        try:
            TaskChangeMethods.change_task_date(login, password, task_id, date_of_start, date_of_end, time_of_start,
                                               time_of_end)
            status = True
            #delete
        except:
            status = False
            self.assertEqual(status, True)

    def test_task_change_date_no_user(self):
        login = "test_user"
        password = "new_test_password"
        task_id = '1'
        date_of_start = "12-05-2017"
        time_of_start = "20:40"
        date_of_end = "15-11-2017"
        time_of_end = "13:21"

        try:
            TaskChangeMethods.change_task_date(login, password, task_id, date_of_start, date_of_end, time_of_start,
                                               time_of_end)
            status = True
            #delete
        except:
            status = False
            self.assertEqual(status, True)

    def test_task_change_date_no_task(self):
        login = "test_user"
        password = "new_test_password"
        task_id = '30'
        date_of_start = "12-05-2017"
        time_of_start = "20:40"
        date_of_end = "15-11-2017"
        time_of_end = "13:21"

        try:
            TaskChangeMethods.change_task_date(login, password, task_id, date_of_start, date_of_end, time_of_start,
                                               time_of_end)
            status = True
            #delete
        except:
            status = False
            self.assertEqual(status, True)

    def test_task_change_status_good(self):
        login = "test_user"
        password = "new_test_password"
        task_id = '10'
        status = 'Stas'

        try:
            TaskChangeMethods.change_status(login, password, task_id, status)
            status = True
            #delete
        except:
            status = False
            self.assertEqual(status, True)

    def test_task_change_status_no_task(self):
        login = "test_user"
        password = "new_test_password"
        task_id = '30'
        status = 'Stas'

        try:
            TaskChangeMethods.change_status(login, password, task_id, status)
            status = True
            # delete
        except:
            status = False
            self.assertEqual(status, True)

    def test_task_change_status_no_user(self):
        login = "testrfrrff_user"
        password = "new_test_password"
        task_id = '10'
        status = 'Stas'

        try:
            TaskChangeMethods.change_status(login, password, task_id, status)
            status = True
            #delete
        except:
            status = False
            self.assertEqual(status, True)

    def test_task_change_status_no_owner(self):
        login = "test_user"
        password = "new_test_password"
        task_id = '1'
        status = 'Stas'

        try:
            TaskChangeMethods.change_status(login, password, task_id, status)
            status = True
            #delete
        except:
            status = False
            self.assertEqual(status, True)

    def test_task_change_expert_good(self):
        login = "test_user"
        password = "new_test_password"
        task_id = '13'
        expert = 'aa'

        try:
            TaskChangeMethods.change_expert(login, password, task_id, expert)
            status = True
            # delete
        except:
            status = False
            self.assertEqual(status, True)

    def test_task_change_expert_no_user(self):
        login = "testtgtgtg_user"
        password = "new_test_password"
        task_id = '13'
        expert = 'aa'

        try:
            TaskChangeMethods.change_expert(login, password, task_id, expert)
            status = True
            # delete
        except:
            status = False
            self.assertEqual(status, True)

    def test_task_change_expert_no_owner(self):
        login = "aa"
        password = "new_test_password"
        task_id = '13'
        expert = 'aa'

        try:
            TaskChangeMethods.change_expert(login, password, task_id, expert)
            status = True
            # delete
        except:
            status = False
            self.assertEqual(status, True)

    def test_task_change_expert_no_task(self):
        login = "test_user"
        password = "new_test_password"
        task_id = '30'
        expert = 'aa'

        try:
            TaskChangeMethods.change_expert(login, password, task_id, expert)
            status = True
            # delete
        except:
            status = False
            self.assertEqual(status, True)

    def test_task_change_task_good(self):
        login = "test_user"
        password = "new_test_password"
        task_id = '11'
        header = 'Stas'
        priority = 'most'
        tags = 'Man'
        comment = 'HOT'

        try:
            TaskChangeMethods.change_task(login, password, task_id, header, priority, tags, comment)
            status = True
            # delete
        except:
            status = False
            self.assertEqual(status, True)

    def test_task_change_task_no_user(self):
        login = "testdfdfdffd_user"
        password = "new_test_password"
        task_id = '11'
        header = 'Stas'
        priority = 'most'
        tags = 'Man'
        comment = 'HOT'

        try:
            TaskChangeMethods.change_task(login, password, task_id, header, priority, tags, comment)
            status = True
            # delete
        except:
            status = False
            self.assertEqual(status, True)

    def test_task_change_task_no_owner(self):
        login = "test_user"
        password = "new_test_password"
        task_id = '1'
        header = 'Stas'
        priority = 'most'
        tags = 'Man'
        comment = 'HOT'

        try:
            TaskChangeMethods.change_task(login, password, task_id, header, priority, tags, comment)
            status = True
            # delete
        except:
            status = False
            self.assertEqual(status, True)

    # def test_task_change_task_good(self):
    #     login = "test_user"
    #     password = "new_test_password"
    #     task_id = '40'
    #     header = 'Stas'
    #     priority = 'most'
    #     tags = 'Man'
    #     comment = 'HOT'
    #
    #     try:
    #         TaskChangeMethods.change_task(login, password, task_id, header, priority, tags, comment)
    #         status = True
    #         # delete
    #     except:
    #         status = False
    #         self.assertEqual(status, True)


class TestsTaskArchive(unittest.TestCase):
    def test_task_group_tags_good(self):
        login = "test_user"
        password = "new_test_password"

        try:
            ArchiveTask.group_by_tags(login, password)
            status = True
            # delete
        except:
            status = False
            self.assertEqual(status, True)

    def test_task_group_tags_no_user(self):
        login = "testrfrfrfrfr_user"
        password = "new_test_password"

        try:
            ArchiveTask.group_by_tags(login, password)
            status = True
            # delete
        except:
            status = False
            self.assertEqual(status, True)

    def test_task_group_status_good(self):
        login = "test_user"
        password = "new_test_password"

        try:
            ArchiveTask.group_by_status(login, password)
            status = True
            # delete
        except:
            status = False
            self.assertEqual(status, True)

    def test_task_group_status_no_user(self):
        login = "testfvfvfvf_user"
        password = "new_test_password"

        try:
            ArchiveTask.group_by_status(login, password)
            status = True
            # delete
        except:
            status = False
            self.assertEqual(status, True)

    def test_task_search_status_good(self):
        login = "test_user"
        password = "new_test_password"
        search_info = 'Done'

        try:
            ArchiveTask.search_by_status(login, password, search_info)
            status = True
            # delete
        except:
            status = False
            self.assertEqual(status, True)

    def test_task_search_status_no_user(self):
        login = "testdcdcdc_user"
        password = "new_test_password"
        search_info = 'Done'

        try:
            ArchiveTask.search_by_status(login, password, search_info)
            status = True
            # delete
        except:
            status = False
            self.assertEqual(status, True)

    def test_task_search_status_empty(self):
        login = "test_user"
        password = "new_test_password"
        search_info = ''

        try:
            ArchiveTask.search_by_status(login, password, search_info)
            status = True
            # delete
        except:
            status = False
            self.assertEqual(status, True)

    def test_task_search_header_good(self):
        login = "test_user"
        password = "new_test_password"
        search_info = 'Стол'

        try:
            ArchiveTask.search_by_status(login, password, search_info)
            status = True
            # delete
        except:
            status = False
            self.assertEqual(status, True)

    def test_task_search_header_no_user(self):
        login = "testdcdcdc_user"
        password = "new_test_password"
        search_info = 'Стул'

        try:
            ArchiveTask.search_by_header(login, password, search_info)
            status = True
            # delete
        except:
            status = False
            self.assertEqual(status, True)

    def test_task_search_header_empty(self):
        login = "test_user"
        password = "new_test_password"
        search_info = ''

        try:
            ArchiveTask.search_by_header(login, password, search_info)
            status = True
            # delete
        except:
            status = False
            self.assertEqual(status, True)

    def test_task_search_tags_good(self):
        login = "test_user"
        password = "new_test_password"
        search_info = 'деревяный'

        try:
            ArchiveTask.search_by_tags(login, password, search_info)
            status = True
            # delete
        except:
            status = False
            self.assertEqual(status, True)

    def test_task_search_tags_no_user(self):
        login = "testdcdcdc_user"
        password = "new_test_password"
        search_info = 'деревяный'

        try:
            ArchiveTask.search_by_tags(login, password, search_info)
            status = True
            # delete
        except:
            status = False
            self.assertEqual(status, True)

    def test_task_search_tags_empty(self):
        login = "test_user"
        password = "new_test_password"
        search_info = ''

        try:
            ArchiveTask.search_by_tags(login, password, search_info)
            status = True
            # delete
        except:
            status = False
            self.assertEqual(status, True)

    def test_task_help(self):
        try:
            help_ru()
            status = True
            # delete
        except:
            status = False
            self.assertEqual(status, True)


class TestsUser(unittest.TestCase):

    def test_user_registration_one(self):
        login = "t_user"
        password = "test_password"
        question = 'Where is your car?'
        answer = 'at home'




        try:
            user = UserMethods.registration(login, password, question, answer)
            status = True
            UsersFunctions.delete_user(login)
        except:
            status = False
            # UsersFunctions.delete_user(login)
            self.assertEqual(status, True)

    def test_user_registration_two(self):
        login = "testerertertertertertertertert_user"
        log = "testerert_user"
        password = "test_password"
        question = 'Where is your car?'
        answer = 'at home'


        user = UserMethods.registration(login, password, question, answer)

        try:
            user = UserMethods.registration(login, password, question, answer)
            status = True
            UsersFunctions.delete_user(login)
        except:
            status = False

            self.assertEqual(status, False)

    def test_user_change_password_one(self):
        ##############################
        login = "tes_user"
        password = "test_password"
        new_password = 'new_test_password'
        question = 'Where is your car?'
        answer = 'at home'

        try:
            UserMethods.change_password(login, password, new_password, question, answer)
            status = True
        except:
            status = False

        self.assertEqual(status, True)

    def test_user_change_password_two(self):
        login = "test_user"
        password = "test_password"
        new_password = 'new_test_password'
        question = 'Where is your car?'
        answer = 'at home'
        user = UserMethods.registration(login, password, question, answer)

        try:
            UserMethods.change_password(login, password, new_password, question, answer)
            status = True
        except:
            status = False

        self.assertEqual(status, True)

    def test_user_change_password_three(self):
        login = "test_user"
        password = "test_password"
        new_password = 'test_password'
        question = 'Where is your car?'
        answer = 'at home'
        user = UserMethods.registration(login, password, question, answer)

        try:
            UserMethods.change_password(login, password, new_password, question, answer)
            status = True
        except:
            status = False
            self.assertEqual(status, True)

