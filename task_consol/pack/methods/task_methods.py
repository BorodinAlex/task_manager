# from sqlalchemy.exc import *
from pack.functions.task_functions import TaskFunctions
from pack.functions.user_functions import UsersFunctions
from pack.db.tables import Task
import logging
from datetime import datetime
# import datetime

logging.basicConfig(filename="journal_methods.log", level=logging.INFO)


class HelpMethods:

    @classmethod
    def check_on_valid_user(cls, login):
        """
        user validation
        :param login: login for registration
        :return: True or False
        """
        users = UsersFunctions.get_user_by_login(login)
        if users is None:
            return False
        else:
            return True

    @classmethod
    def check_on_valid_task(cls, id):
        """
        task validation
        :param id: id of the specific task
        :return: True or False
        """
        task = TaskFunctions.get_task_by_id(id)
        if task is None:
            return False
        else:
            return True

    @classmethod
    def check_datetime(cls, date_of_start, date_of_end, time_of_start, time_of_end):
        """
        date and time verification function
        :param date_of_start: the start date of the alert
        :param date_of_end: the end date of the notification
        :param time_of_start: task start time
        :param time_of_end: task end time
        :return: True or False
        """
        try:
            datetime.strptime(date_of_start, '%d-%m-%Y')
            datetime.strptime(date_of_end, '%d-%m-%Y')
            datetime.strptime(time_of_start, '%H:%M')
            datetime.strptime(time_of_end, '%H:%M')
            return True
        except:
            return False

    @classmethod
    def delete_sub_string_in_string(cls, string, sub_string):
        """
        #########################################
        :param string:
        :param sub_string:
        :return:
        """
        work_string = string.split(',')
        try:
            work_string.remove(str(sub_string))
            work_string = list(filter(None, work_string))
            new_string = ''
            for ids in work_string:
                new_string += ids + ','
            return new_string
        except:
            pass

    @classmethod
    def find_sub_sting_in_string(cls, string, substring):
        """
        #######################################
        :param string:
        :param substring:
        :return:
        """
        new_list = string.split(',')
        new_list = list(filter(None, new_list))
        try:
            new_list.index(str(substring))
            return True
        except:
            return False

    @classmethod
    def is_task_have_parent(cls, id):
        """
        check if the task has a parent
        :param id: id of the specific task
        :return: True or False
        """

        task = TaskFunctions.get_task_by_id(id)
        if task.parent_task_id == 0:
            return False
        else:
            return True

    @classmethod
    def task_is_under_task(cls, id):
        """
        heck if the task is under task
        :param id: id of the specific task
        :return: True or False
        """
        task = TaskFunctions.get_task_by_id(id)
        if task.is_under_task == 'Yes':
            return False
        else:
            return True

    @classmethod
    def check_under_task_and_parent_task_linked(cls, parent_id, child_id):
        """
        ################################
        :param parent_id: id of the parent task
        :param child_id: id of the child task
        :return:True or False
        """
        child_task = TaskFunctions.get_task_by_id(child_id)
        if child_task.parent_task_id == int(parent_id):
            return True
        else:
            return False

    @classmethod
    def is_tasks_already_linked(cls, parent_id, child_id):
        """
        check, linked to the task
        :param parent_id: id of the parent task
        :param child_id: id of the child task
        :return:True or False
        """
        parent_task = TaskFunctions.get_task_by_id(parent_id)
        child_task = TaskFunctions.get_task_by_id(child_id)
        left_link = HelpMethods.find_sub_sting_in_string(parent_task.linked_task_id, child_id)
        right_link = HelpMethods.find_sub_sting_in_string(child_task.linked_task_id, parent_id)
        if left_link is True and right_link is True:
            return True
        else:
            return False


class TaskAddMethods:

    @classmethod
    def add_task(cls, login, password, header, priority, tags, comment, date_of_start, time_of_start,
                 date_of_end, time_of_end, status):
        """
        adding a new single task to the database
        :param login:  login for registration
        :param password: password to register
        :param header: the name of the task
        :param priority: tasks priority
        :param tags: tag / task group
        :param comment: comment to the task
        :param date_of_start: the start date of the alert
        :param date_of_end: the end date of the notification
        :param time_of_start: task start time
        :param time_of_end: task end time
        :param status: is the current status of the task.Can be set to 'Done', 'Not performed', 'Pending execution'
        :return: single task in db
        """
        # try:
        user = UsersFunctions.get_user_by_login_and_password(login, password)
        if datetime.strptime(date_of_end, '%d-%m-%Y') < datetime.strptime(date_of_start, '%d-%m-%Y'):
               print("Дата окончания не должна превышать дату начала")
        elif user is None:
             print("Пароль или логин введен неверно")
        else:
            correct_date = HelpMethods.check_datetime(date_of_start, date_of_end, time_of_start, time_of_end)
            if correct_date:
                task = Task(owner=login, header=header, priority=priority, tags=tags, comment=comment,
                            date_of_start=date_of_start,
                            time_of_start=time_of_start, date_of_end=date_of_end, time_of_end=time_of_end,
                            status=status)
                TaskFunctions.add_task(task)
                print("Задача добавлена")
                    # logging.info(" %s: Successful task adding, was created task '%s' by %s (function: %s)" %
                    #              (datetime.datetime.now(), header, login, 'add_task()'))
        # except:
        #     logging.warning(" %s: Error with task adding, was not created task '%s' by %s (function: %s)" %
        #                     (datetime.datetime.now(), header, login, 'add_task()'))
        #     print("Одна из дат или время введены неверно, формат даты '11-11-2011', формат время '16:40'")


class TaskDeleteMethods:

    @classmethod
    def parse_and_delete_id_from_linked_task(cls, linked_task_id, deleted_id):
        """
        #####################################
        :param linked_task_id:
        :param deleted_id:
        :return:
        """
        try:
            old_task = TaskFunctions.get_task_by_id(linked_task_id)
            new_linked_id_string = HelpMethods.delete_sub_string_in_string(old_task.linked_task_id, deleted_id)
            if new_linked_id_string == '':
                task = Task(id=linked_task_id, is_linked='None', linked_task_id='None')
                TaskFunctions.actions_linked(task)
            else:
                task = Task(id=linked_task_id, is_linked='Yes', linked_task_id=new_linked_id_string)
                TaskFunctions.actions_linked(task)
        except:
            pass

    @classmethod
    def delete_all_childs_of_task(cls, task_id):
        """
        function to delete all sub tasks of task
        :param task_id: id of the specific task
        :return: delete all sub tasks of task
        """
        try:
            tasks = TaskFunctions.get_all_task()
            for delete_task in tasks:
                if delete_task.parent_task_id == int(task_id):
                    TaskFunctions.delete_task(delete_task.id)
        except:
            pass

    @classmethod
    def delete_form_childs_task_id(cls, task_id):
        """
        #######################################
        :param task_id: id of the specific task
        :return:
        """
        try:
            tasks = TaskFunctions.get_all_task()
            for delete_task in tasks:
                is_task_child = HelpMethods.find_sub_sting_in_string(delete_task.under_task_id, task_id)
                if is_task_child:
                    new_under_task_id = HelpMethods.delete_sub_string_in_string(delete_task.under_task_id, task_id)
                    if new_under_task_id == '':
                        task = Task(id=delete_task.id, under_task_id='None', is_parent_task='None', expert='None')
                        TaskFunctions.actions_child(task)
                    else:
                        task = Task(id=delete_task.id, under_task_id=new_under_task_id, is_parent_task='Yes')
                        TaskFunctions.actions_child(task)
        except:
            pass

    @classmethod
    def delete_task(cls, login, password, task_id):
        """
        delete single task from db
        :param login:  login for registration
        :param password: password to register
        :param task_id: id of the specific task
        :return: delete single task
        """
        try:
            user = UsersFunctions.get_user_by_login_and_password(login, password)
            task = TaskFunctions.get_task_by_id(task_id)
            if user is None:
                print("Пароль или логин введен неверно")
            elif task is None:
                print("Данной задачи не существует")
            elif task.owner != login:
                print("Вы не являетесь создателем данной задачи")
            else:
                TaskFunctions.delete_task(task_id)
                TaskDeleteMethods.parse_and_delete_id_from_linked_task(task.linked_task_id, task_id)
                TaskDeleteMethods.delete_all_childs_of_task(task_id)
                TaskDeleteMethods.delete_form_childs_task_id(task_id)
                logging.info(" %s: Successful task deleting, was deleted task with id '%s' by %s (function: %s)" %
                             (datetime.datetime.now(), task_id, login, 'delete_task()'))
        except:
            logging.warning(" %s: Error with task deleting, was not deleted task with id '%s' by %s (function: %s)" %
                            (datetime.datetime.now(), task_id, login, 'delete_task()'))


class TaskChangeMethods:

    @classmethod
    def change_task_date(cls, login, password, id, date_of_start, date_of_end, time_of_start, time_of_end):
        """
        function to change the date and time of the task
        :param login:  login for registration
        :param password: password to register
        :param id: id of the specific task
        :param date_of_start: the start date of the alert
        :param date_of_end: the end date of the notification
        :param time_of_start: task start time
        :param time_of_end: task end time
        :return: changed task date
        """
        # try:
        user = UsersFunctions.get_user_by_login_and_password(login, password)
        task = TaskFunctions.get_task_by_id(id)
        if user is None:
            print("Пароль или логин введен неверно")
        elif task is None:
            print("Данной задачи не существует")
        elif datetime.strptime(date_of_end, '%d-%m-%Y') < datetime.strptime(date_of_start, '%d-%m-%Y'):
            print("Дата окончания не должна превышать дату начала")
        elif task.owner != login:
            print("Вы не являетесь создателем данной задачи")
        else:
            correct_date = HelpMethods.check_datetime(date_of_start, date_of_end, time_of_start, time_of_end)
            if correct_date:
                task = Task(id=id, date_of_start=date_of_start, time_of_start=time_of_start,
                               date_of_end=date_of_end, time_of_end=time_of_end)
                TaskFunctions.change_date_time_task(task)
                # logging.info(
                #     " %s: Successful changing task date, was changed date of task with id '%s' by %s (function: %s)" %
                #     (datetime.datetime.now(), id, login, 'change_task_date()'))
        # except:
        #     logging.warning("%s: Error with changing task date, was not changed date of task with id '%s' by %s "
        #                     "(func: %s)" % (datetime.datetime.now(), id, login, 'change_task_date()'))
            else:
                print("Неверно введены дата или время")

    @classmethod
    def change_expert(cls, login, password, id, expert):
        import datetime
        """
        function change task expert
        :param login:  login for registration
        :param password: password to register
        :param id: id of the specific task
        :param expert: expert of the task
        :return: changed task
        """
        try:
            user = UsersFunctions.get_user_by_login_and_password(login, password)
            is_valid_task = TaskFunctions.get_task_by_id(id)
            is_valid_user = HelpMethods.check_on_valid_user(expert) #############################################################
            if user is None:
                print("Пароль или логин введен неверно")
            elif is_valid_task is None:
                print("Данной задачи не существует")
            elif is_valid_task.owner != login:
                print("Вы не являетесь создателем данной задачи")
            else:
                is_task_under_task = HelpMethods.task_is_under_task(id)
                if is_task_under_task:
                    task = Task(id=id, expert=expert)
                    TaskFunctions.change_expert(task)
            logging.info(
                " %s: Successful changing task expert, was changed expert of task with id '%s' by %s (func: %s)" %
                (datetime.datetime.now(), id, login, 'change_expert()'))
        except:
            logging.warning("%s: Error with changing task expert, wasn't changed expert of task: id '%s' by %s "
                            "(func: %s)" % (datetime.datetime.now(), id, login, 'change_expert()'))
            print("Данная задача не является подзадачей, вы не можете присвоит ей исполняющего")

    @classmethod
    def split_string(cls, string):
        """
        function to split strings
        :param string: some string
        :return: splitted string
        """
        tasks_id = string.split(',')
        tasks_id = list(filter(None, tasks_id))
        return tasks_id

    @classmethod
    def change_parent_task(cls, id):
        """
        change parent for task
        :param id: id of the specific task
        :return: change parent for task
        """
        try:
            child_task = TaskFunctions.get_task_by_id(id)
            parent_task = TaskFunctions.get_task_by_id(child_task.parent_task_id)
            child_id_of_parent_task = TaskChangeMethods.split_string(parent_task.under_task_id)
            count_of_complete_task = TaskFunctions.count_of_complite_task(parent_task.id)
            if len(child_id_of_parent_task) == count_of_complete_task:
                new_tasks = Task(id=parent_task.id, status='Выполнено')
                TaskFunctions.change_task_status(new_tasks)
            else:
                new_tasks = Task(id=parent_task.id, status='В процессе выполнения')
                TaskFunctions.change_task_status(new_tasks)
        except:
            pass

    @classmethod
    def change_status_of_childs_tasks(cls, id):
        """
        function to change the status of sub tasks
        :param id: id of the specific task
        :return: new status of childs tasks
        """
        try:
            all_tasks = TaskFunctions.get_all_task()
            for task in all_tasks:
                if task.parent_task_id == id:
                    update_task = Task(id=task.id, status="Выполнено")
                    TaskFunctions.change_task_status(update_task)
        except:
            pass

    @classmethod
    def change_status(cls, login, password, id, status):
        import datetime
        """
        task status change function
        :param login:  login for registration
        :param password: password to register
        :param id: id of the specific task
        :param status: is the current status of the task.Can be set to 'Done', 'Not performed', 'Pending execution'
        :return: new task status
        """
        try:
            user = UsersFunctions.get_user_by_login_and_password(login, password)
            task = TaskFunctions.get_task_by_id(id)
            if user is None:
                print("Пароль или логин введен неверно")
            elif task is None:
                print("Данной задачи не существует")
            elif task.owner != login:
                print("Вы не являетесь создателем данной задачи")
            else:
                task = Task(id=id, status=status)
                TaskFunctions.change_task_status(task)
                if status == 'Выполнено':
                    TaskChangeMethods.change_status_of_childs_tasks(id)
                    TaskChangeMethods.change_parent_task(id)
            logging.info(" %s: Successful changing task status, was changed status of task with id '%s' by %s "
                         "(func: %s)" % (datetime.datetime.now(), id, login, 'change_status()'))
        except:
            logging.warning("%s: Error with changing task status, wasn't changed status of task: id '%s' by %s "
                            "(func: %s)" % (datetime.datetime.now(), id, login, 'change_status()'))

    @classmethod
    def change_task(cls, login, password, id, header, priority, tags, comment):
        import datetime
        """
        task change function
        :param login:  login for registration
        :param password: password to register
        :param header: the name of the task
        :param priority: tasks priority
        :param id: id of the specific task
        :param tags: tag / task group
        :param comment: comment to the task
        :return: changed task

        """
        try:
            is_user_valid = UsersFunctions.get_user_by_login_and_password(login, password)
            is_task_valid = TaskFunctions.get_task_by_id(id)
            if is_user_valid is None:
                print("Пароль или логин введен неверно")
            elif is_task_valid is None:
                print("Данной задачи не существует")
            elif is_task_valid.owner != login:
                print("Вы не являетесь создателем данной задачи")
            else:
                task = Task(id=id, header=header, priority=priority, tags=tags, comment=comment)
                TaskFunctions.change_task(task)
            logging.info(" %s: Successful changing task, was changed task with id '%s' by %s (func: %s)" %
                         (datetime.datetime.now(), id, login, 'change_task()'))
        except:
            logging.warning("%s: Error with changing task, wasn't changed task: id '%s' by %s (func: %s)" %
                            (datetime.datetime.now(), id, login, 'change_task()'))


class LinkedTaskMethods:

    @classmethod
    def add_linked_task(cls, login, password, id, linked_task_id):
        """
        ####################
        :param login:  login for registration
        :param password: password to register
        :param id: id of the specific task
        :param linked_task_id: ###########################
        :return: linked task
        """
        try:
            is_valid_user = UsersFunctions.get_user_by_login_and_password(login, password)
            left_task = TaskFunctions.get_task_by_id(id)
            right_task = TaskFunctions.get_task_by_id(linked_task_id)
            is_task_already_linked = HelpMethods.is_tasks_already_linked(id, linked_task_id)
            if is_valid_user is None:
                print("Пароль или логин введен неверно")
            elif left_task is None:
                print("Данной задачи не существует")
            elif right_task is False:
                print("Задачи, с которой вы пытаетесь связать, не существует")
            elif right_task.owner != login or left_task.owner != login:
                print("Вы не являетесь владельцем данных задач")
            elif id == linked_task_id:
                print("Вы не можете связать задачу с самой сабой")
            elif is_task_already_linked is True:
                print("Задачи уже связаны")
            else:
                if left_task.linked_task_id == 'None':
                    new_task = Task(id=left_task.id, linked_task_id=str(right_task.id) + ',', is_linked='Yes')
                    TaskFunctions.actions_linked(new_task)
                else:
                    new_task = Task(id=left_task.id, linked_task_id=left_task.linked_task_id + str(right_task.id) + ',',
                                    is_linked='Yes')
                    TaskFunctions.actions_linked(new_task)
                if right_task.linked_task_id == 'None':
                    new_task = Task(id=right_task.id, linked_task_id=str(left_task.id) + ',', is_linked='Yes')
                    TaskFunctions.actions_linked(new_task)
                else:
                    new_task = Task(id=right_task.id, linked_task_id=right_task.linked_task_id + str(left_task.id) + ',',
                                    is_linked='Yes')
                    TaskFunctions.actions_linked(new_task)
            logging.info(" %s: Successful adding linked task, was  added linked task with id '%s' by %s (func: %s)" %
                         (datetime.datetime.now(), id, login, 'add_linked_task()'))
        except:
            logging.warning("%s: Error with adding linked task, wasn't added task: id '%s' by %s (func: %s)" %
                            (datetime.datetime.now(), id, login, 'add_linked_task()'))

    @classmethod
    def delete_link_between_linked_task(cls, login, password, id, linked_task_id):
        """
        ####################
        :param login:  login for registration
        :param password: password to register
        :param id: id of the specific task
        :param linked_task_id: ###########################
        :return: delete linked task
        """
        try:
            is_valid_user = UsersFunctions.get_user_by_login_and_password(login, password)
            left_task = TaskFunctions.get_task_by_id(id)
            right_task = TaskFunctions.get_task_by_id(linked_task_id)
            is_task_already_linked = HelpMethods.is_tasks_already_linked(id, linked_task_id)
            new_linked_str_for_left_task = HelpMethods.delete_sub_string_in_string(left_task.linked_task_id, linked_task_id)
            new_linked_str_for_right_task = HelpMethods.delete_sub_string_in_string(right_task.linked_task_id, id)
            if is_valid_user is None:
                print("Пароль или логин введен неверно")
            elif left_task is None:
                print("Данной задачи не существует")
            elif right_task is False:
                print("Задачи, с которой вы пытаетесь связать, не существует")
            elif right_task.owner != login or left_task.owner != login:
                print("Вы не являетесь владельцем данных задач")
            elif id == linked_task_id:
                print("Вы не можете связать задачу с самой сабой")
            elif is_task_already_linked is not True:
                print("Задачи не связаны")
            else:
                if new_linked_str_for_left_task == '':
                    new_task = Task(id=left_task.id, is_linked='None', linked_task_id='None')
                    TaskFunctions.actions_linked(new_task)
                else:
                    new_task = Task(id=left_task.id, is_linked='Yes', linked_task_id=new_linked_str_for_left_task)
                    TaskFunctions.actions_linked(new_task)
                if new_linked_str_for_right_task == '':
                    new_task = Task(id=right_task.id, is_linked='None', linked_task_id='None')
                    TaskFunctions.actions_linked(new_task)
                else:
                    new_task = Task(id=right_task.id, is_linked='Yes', linked_task_id=new_linked_str_for_right_task)
                    TaskFunctions.actions_linked(new_task)
            logging.info(" %s: Successful deleting of link, was not deleting linked task with id '%s' by %s (func: %s)" %
                         (datetime.datetime.now(), id, login, 'delete_link_between_linked_task()'))
        except:
            logging.warning("%s: Error with deleting link, wasn't deleted tasks link: id '%s' by %s (func: %s)" %
                            (datetime.datetime.now(), id, login, 'delete_link_between_linked_task()'))


class ParentTaskMethods:

    @classmethod
    def add_parent_for_task(cls, login, password, id, parent_id, expert):
        """
        add parent for task function
        :param login:  login for registration
        :param password: password to register
        :param id: id of the specific task
        :param parent_id: id of the parent task
        :param expert: expert of the task
        :return: task with a parent
        """

        try:
            is_valid_user = UsersFunctions.get_user_by_login_and_password(login, password)
            is_child_has_parent_task = HelpMethods.is_task_have_parent(id)
            parent_task = TaskFunctions.get_task_by_id(parent_id)
            child_task = TaskFunctions.get_task_by_id(id)
            is_tasks_already_linked = HelpMethods.check_under_task_and_parent_task_linked(parent_id, id)
            if is_valid_user is None:
                print("Пароль или логин введен неверно")
            if parent_task.owner != login or child_task.owner != login:
                print("Вы не являетесь создателем данных задач")
            if parent_task.is_parent_task == "Yes":
                print("Родительская задача уже является родителем")
            if id == parent_id:
                print("Вы не можете пометить задаче как родительскую саму себя")
            if is_child_has_parent_task:
                print("У данной задачи уже есть родитель")
            if is_tasks_already_linked:
                print("Данные задачи уже связаны")
            if parent_task is None or child_task is None:
                print("Одной из задач не существует")
            else:
                if parent_task.under_task_id == 'None' and parent_task.is_parent_task == 'None':
                    new_parent_task = Task(id=parent_id, under_task_id=str(id) + ',', is_parent_task='Yes')
                    TaskFunctions.actions_parent(new_parent_task)
                    new_child_task = Task(id=id, parent_task_id=parent_id, is_under_task='Yes', expert=expert)
                    TaskFunctions.actions_child(new_child_task)
                else:
                    new_parent_task = Task(id=parent_id, under_task_id=parent_task.under_task_id + str(id) + ',',
                                           is_parent_task='Yes')
                    TaskFunctions.actions_parent(new_parent_task)
                    new_child_task = Task(id=id, parent_task_id=int(id), is_under_task='Yes', expert=str(expert))
                    TaskFunctions.actions_child(new_child_task)
            logging.info(" %s: Successful adding parent for task, was added parent for task with id '%s' by %s "
                         "(func: %s)" % (datetime.datetime.now(), id, login, 'add_parent_for_task()'))
        except:
            logging.warning("%s: Error with adding parent for task, wasn't added parent for task: id '%s' "
                            "by %s (func: %s)" %
                            (datetime.datetime.now(), id, login, 'add_parent_for_task()'))

    @classmethod
    def delete_link_between_task(cls, login, password, id, parent_id):
        """
        delete link between task function
        :param login:  login for registration
        :param password: password to register
        :param id: id of the specific task
        :param parent_id: id of the parent task
        :return: delete link between tasks
        """
        try:
            is_valid_user = UsersFunctions.get_user_by_login_and_password(login, password)
            is_child_has_parent_task = HelpMethods.check_under_task_and_parent_task_linked(id, parent_id)
            parent_task = TaskFunctions.get_task_by_id(parent_id)
            child_task = TaskFunctions.get_task_by_id(id)
            new_str_of_id_for_parent_task = HelpMethods.delete_sub_string_in_string(parent_task.under_task_id, id)
            if parent_task is None or child_task is None:
                print("Одной из задач не существует")
            if is_valid_user is None:
                print("Пароль или логин введен неверно")
            if parent_task.owner != login or child_task.owner != login:
                print("Вы не являетесь создателем данных задач")
            if parent_task.is_parent_task == "Yes":
                print("Родительская задача уже является родителем")
            elif is_child_has_parent_task is False:
                print("У подзадачи нет родителя")
            else:
                new_child_task = Task(id=id, parent_task_id=0, is_under_task='None', expert='None')
                TaskFunctions.actions_child(new_child_task)
                if new_str_of_id_for_parent_task == '':
                    new_parent_task = Task(id=parent_id, is_parent_task='None', under_task_id='None')
                    TaskFunctions.actions_parent(new_parent_task)
                else:
                    new_parent_task = Task(id=parent_id, is_parent_task='Yes', under_task_id=new_str_of_id_for_parent_task)
                    TaskFunctions.actions_parent(new_parent_task)
            logging.info(" %s: Successful deleting of link, was deleting linked task with id '%s' by %s (func: %s)" %
                         (datetime.datetime.now(), id, login, 'delete_link_between_task()'))
        except:
            logging.warning("%s: Error with deleting link, wasn't deleted tasks link: id '%s' by %s (func: %s)" %
                            (datetime.datetime.now(), id, login, 'delete_link_between_task()'))


class ArchiveTask:

    @classmethod
    def is_task_archive(cls, task):
        from _datetime import datetime

        """
        check to see if task is in the archive
        :param task: object task that is stored in the database
        :return: True or False
        """
        date = TaskFunctions.get_task_by_id(id=task.id)
        current_date = datetime.now()
        if datetime.strptime(str(date.date_of_end), '%d-%m-%Y') < current_date:
            return True
        else:
            return False

    @classmethod
    def print_archive(cls, login, password):
        """
        print function
        :param login:  login for registration
        :param password: password to register
        :return: print tasks from archive
        """
        try:
            tasks = TaskFunctions.get_all_task()
            is_valid_user = UsersFunctions.get_user_by_login_and_password(login, password)
            if is_valid_user is None:
                print("Пароль или логин введен неверно")
            else:
                for task in tasks:
                    is_archive = ArchiveTask.is_task_archive(task)
                    if is_archive is True and (task.owner == login or task.expert == login):
                        print(('id: {0} \n'
                               'Название: {1} \n'
                               'Приоритет: {2} \n'
                               "Группа: {3} \n"
                               "Комментарий: {4} \n"
                               "Дата и время создания: {5} {6} \n"
                               "Дата и время начала: {7} {8} \n"
                               "Дата и время окончания: {9} {10} \n"
                               "Выполняющий: {11} \n"
                               "Является ли задача связанной: {12} \n"
                               "Связана с: {13} \n"
                               "Является подзадачей: {14} \n"
                               "id Родительской задачи: {15} \n"
                               "Является ли родительской задачей: {16} \n").format(task.id, task.header, task.priority,
                                                                                   task.tags, task.comment,
                                                                                   task.date_of_create, task.time_of_create,
                                                                                   task.date_of_start, task.time_of_start,
                                                                                   task.date_of_end, task.time_of_end,
                                                                                   task.expert, task.is_linked,
                                                                                   task.linked_task_id, task.is_under_task,
                                                                                   task.parent_task_id,
                                                                                   task.is_parent_task))
            logging.info(" %s: Successful printing archive, was printed by %s (func: %s)" %
                         (datetime.datetime.now(), login, 'change_expert()'))
        except:
            logging.warning("%s: Error with archive printing, wasn't printed archives by %s (func: %s)" %
                            (datetime.datetime.now(), login, 'print_archive()'))

    @classmethod
    def print_non_archived_tasks(cls, login, password):
        """
        print function
        :param login:  login for registration
        :param password: password to register
        :return: print non archive tasks
        """
        try:
            tasks = TaskFunctions.get_all_task()
            is_valid_user = UsersFunctions.get_user_by_login_and_password(login, password)
            if is_valid_user is None:
                print("Пароль или логин введен неверно")
            else:
                for task in tasks:
                    is_archive = ArchiveTask.is_task_archive(task)
                    if is_archive is False and (task.owner == login or task.expert == login):
                        print(('id: {0} \n'
                               'Название: {1} \n'
                               'Приоритет: {2} \n'
                               "Группа: {3} \n"
                               "Комментарий: {4} \n"
                               "Дата и время создания: {5} {6} \n"
                               "Дата и время начала: {7} {8} \n"
                               "Дата и время окончания: {9} {10} \n"
                               "Выполняющий: {11} \n"
                               "Является ли задача связанной: {12} \n"
                               "Связана с: {13} \n"
                               "Является подзадачей: {14} \n"
                               "id Родительской задачи: {15} \n"
                               "Является ли родительской задачей: {16} \n").format(task.id, task.header, task.priority,
                                                                                   task.tags, task.comment,
                                                                                   task.date_of_create,
                                                                                   task.time_of_create, task.date_of_start,
                                                                                   task.time_of_start,
                                                                                   task.date_of_end, task.time_of_end,
                                                                                   task.expert, task.is_linked,
                                                                                   task.linked_task_id, task.is_under_task,
                                                                                   task.parent_task_id,
                                                                                   task.is_parent_task))
            logging.info(" %s: Successful printing non archive tasks, was printed by %s (func: %s)" %
                         (datetime.datetime.now(), login, 'print_non_archived_tasks()'))
        except:
            logging.warning("%s: Error with non archive printing, wasn't printed non archives tasks by %s (func: %s)" %
                            (datetime.datetime.now(), login, 'print_archive()'))

    @classmethod
    def group_by_tags(cls, login, password):
        """
        grouping by tag function
        :param login:  login for registration
        :param password: password to register
        :return: grouped tasks by tag
        """

        tasks = TaskFunctions.group_by_tags()
        is_valid_user = UsersFunctions.get_user_by_login_and_password(login, password)
        if is_valid_user is None:
            print("Пароль или логин введен неверно")
        else:
            for task in tasks:
                is_archive = ArchiveTask.is_task_archive(task)
                if is_archive is False and (task.owner == login or task.expert == login):
                    print(('id: {0} \n'
                            'Название: {1} \n'
                               'Приоритет: {2} \n'
                               "Группа: {3} \n"
                               "Комментарий: {4} \n"
                               "Дата и время создания: {5} {6} \n"
                               "Дата и время начала: {7} {8} \n"
                               "Дата и время окончания: {9} {10} \n"
                               "Выполняющий: {11} \n"
                               "Является ли задача связанной: {12} \n"
                               "Связана с: {13} \n"
                               "Является подзадачей: {14} \n"
                               "id Родительской задачи: {15} \n"
                               "Является ли родительской задачей: {16} \n").format(task.id, task.header, task.priority,
                                                                                   task.tags, task.comment,
                                                                                   task.date_of_create,
                                                                                   task.time_of_create, task.date_of_start,
                                                                                   task.time_of_start,
                                                                                   task.date_of_end, task.time_of_end,
                                                                                   task.expert, task.is_linked,
                                                                                   task.linked_task_id, task.is_under_task,
                                                                                   task.parent_task_id,
                                                                                   task.is_parent_task))

        #     logging.info(" %s: Successful grouping tasks by tags, was grouped tasks by tags by %s (func: %s)" %
        #                  (datetime.datetime.now(), login, 'group_by_tags()'))
        # except:
        #     logging.warning("%s: Error with grouping tasks by tags, wasn't grouping tasks by %s (func: %s)" %
        #                     (datetime.datetime.now(), login, 'group_by_tags()'))

    @classmethod
    def group_by_status(cls, login, password):
        """
        grouping by status function
        :param login:  login for registration
        :param password: password to register
        :return: grouped tasks by status
        """
        try:
            tasks = TaskFunctions.group_by_status()
            is_valid_user = UsersFunctions.get_user_by_login_and_password(login, password)
            if is_valid_user is None:
                print("Пароль или логин введен неверно")
            else:
                for task in tasks:
                    is_archive = ArchiveTask.is_task_archive(task)
                    if is_archive is False and (task.owner == login or task.expert == login):
                        print(('id: {0} \n'
                               'Название: {1} \n'
                               'Приоритет: {2} \n'
                               "Группа: {3} \n"
                               "Комментарий: {4} \n"
                               "Дата и время создания: {5} {6} \n"
                               "Дата и время начала: {7} {8} \n"
                               "Дата и время окончания: {9} {10} \n"
                               "Выполняющий: {11} \n"
                               "Является ли задача связанной: {12} \n"
                               "Связана с: {13} \n"
                               "Является подзадачей: {14} \n"
                               "id Родительской задачи: {15} \n"
                               "Является ли родителской задачей:{16} \n").format(task.id, task.header, task.priority,
                                                                                 task.tags, task.comment,
                                                                                 task.date_of_create,
                                                                                 task.time_of_create, task.date_of_start,
                                                                                 task.time_of_start,
                                                                                 task.date_of_end, task.time_of_end,
                                                                                 task.expert, task.is_linked,
                                                                                 task.linked_task_id, task.is_under_task,
                                                                                 task.parent_task_id,
                                                                                 task.is_parent_task))
            logging.info(" %s: Successful grouping tasks by status, was grouped tasks by status by %s (func: %s)" %
                         (datetime.datetime.now(), login, 'group_by_status()'))
        except:
            logging.warning("%s: Error with grouping tasks by status, wasn't grouping tasks by %s (func: %s)" %
                            (datetime.datetime.now(), login, 'group_by_status()'))

    @classmethod
    def search_by_header(cls, login, password, search_info):
        """
        task search function by name
        :param login:  login for registration
        :param password: password to register
        :param search_info: search criteria - header
        :return: found during the search tasks
        """
        try:

            tasks = TaskFunctions.search_by_header(search_info)
            is_valid_user = UsersFunctions.get_user_by_login_and_password(login, password)
            if is_valid_user is None:
                print("Пароль или логин введен неверно")
            else:
                for task in tasks:
                    is_archive = ArchiveTask.is_task_archive(task)
                    if is_archive is False and (task.owner == login or task.expert == login):
                        print(('id: {0} \n'
                               'Название: {1} \n'
                               'Приоритет: {2} \n'
                               "Группа: {3} \n"
                               "Комментарий: {4} \n"
                               "Дата и время создания: {5} {6} \n"
                               "Дата и время начала: {7} {8} \n"
                               "Дата и время окончания: {9} {10} \n"
                               "Выполняющий: {11} \n"
                               "Является ли задача связанной: {12} \n"
                               "Связана с: {13} \n"
                               "Является подзадачей: {14} \n"
                               "id Родительской задачи: {15} \n"
                               "Является ли родительской задачей: {16} \n").format(task.id, task.header, task.priority,
                                                                                   task.tags, task.comment,
                                                                                   task.date_of_create,
                                                                                   task.time_of_create, task.date_of_start,
                                                                                   task.time_of_start,
                                                                                   task.date_of_end, task.time_of_end,
                                                                                   task.expert, task.is_linked,
                                                                                   task.linked_task_id, task.is_under_task,
                                                                                   task.parent_task_id,
                                                                                   task.is_parent_task))
            logging.info(" %s: Successful searching tasks by header, was searched tasks by header '%s' by %s (func: %s)" %
                         (datetime.datetime.now(), search_info, login, 'search_by_header()'))
        except:
            logging.warning("%s: Error with searching tasks by header, wasn't searched tasks by header "
                            "'%s' by %s (func: %s)" %
                            (datetime.datetime.now(), search_info, login, 'search_by_header()'))

    @classmethod
    def search_by_tags(cls, login, password, search_info):
        """
        task search function by name
        :param login:  login for registration
        :param password: password to register
        :param search_info: search criteria - header
        :return: found during the search tasks
        """
        try:
            tasks = TaskFunctions.search_by_header(search_info)
            is_valid_user = UsersFunctions.get_user_by_login_and_password(login, password)
            if is_valid_user is None:
                print("Пароль или логин введен неверно")
            else:
                for task in tasks:
                    is_archive = ArchiveTask.is_task_archive(task)
                    if is_archive is False and (task.owner == login or task.expert == login):
                        print(('id: {0} \n'
                               'Название: {1} \n'
                               'Приоритет: {2} \n'
                               "Группа: {3} \n"
                               "Комментарий: {4} \n"
                               "Дата и время создания: {5} {6} \n"
                               "Дата и время начала: {7} {8} \n"
                               "Дата и время окончания: {9} {10} \n"
                               "Выполняющий: {11} \n"
                               "Является ли задача связанной: {12} \n"
                               "Связана с: {13} \n"
                               "Является подзадачей: {14} \n"
                               "id Родительской задачи: {15} \n"
                               "Является ли родительской задачей: {16} \n").format(task.id, task.header, task.priority,
                                                                                   task.tags, task.comment,
                                                                                   task.date_of_create,
                                                                                   task.time_of_create, task.date_of_start,
                                                                                   task.time_of_start,
                                                                                   task.date_of_end, task.time_of_end,
                                                                                   task.expert, task.is_linked,
                                                                                   task.linked_task_id, task.is_under_task,
                                                                                   task.parent_task_id,
                                                                                   task.is_parent_task))
            logging.info(" %s: Successful searching tasks by tags, was searched tasks by tags '%s' by %s (func: %s)" %
                         (datetime.datetime.now(), search_info, login, 'search_by_tags()'))
        except:
            logging.warning("%s: Error with searching tasks by tags, wasn't searched tasks by tags "
                            "'%s' by %s (func: %s)" %
                            (datetime.datetime.now(), search_info, login, 'search_by_tags()'))

    @classmethod
    def search_by_status(cls, login, password, search_info):
        """
        task search function by name
        :param login:  login for registration
        :param password: password to register
        :param search_info: search criteria - header
        :return: found during the search tasks
        """
        try:
            tasks = TaskFunctions.search_by_header(search_info)
            is_valid_user = UsersFunctions.get_user_by_login_and_password(login, password)
            if is_valid_user is None:
                print("Пароль или логин введен неверно")
            else:
                for task in tasks:
                    is_archive = ArchiveTask.is_task_archive(task)
                    if is_archive is False and (task.owner == login or task.expert == login):
                        print(('id: {0} \n'
                               'Название: {1} \n'
                               'Приоритет: {2} \n'
                               "Группа: {3} \n"
                               "Комментарий: {4} \n"
                               "Дата и время создания: {5} {6} \n"
                               "Дата и время начала: {7} {8} \n"
                               "Дата и время окончания: {9} {10} \n"
                               "Выполняющий: {11} \n"
                               "Является ли задача связанной: {12} \n"
                               "Связана с: {13} \n"
                               "Является подзадачей: {14} \n"
                               "id Родительской задачи: {15} \n"
                               "Является ли родительской задачей: {16} \n").format(task.id, task.header, task.priority,
                                                                                   task.tags, task.comment,
                                                                                   task.date_of_create,
                                                                                   task.time_of_create, task.date_of_start,
                                                                                   task.time_of_start,
                                                                                   task.date_of_end, task.time_of_end,
                                                                                   task.expert, task.is_linked,
                                                                                   task.linked_task_id, task.is_under_task,
                                                                                   task.parent_task_id,
                                                                                   task.is_parent_task))
            logging.info(" %s: Successful searching tasks by status, was searched tasks by status '%s' by %s (func: %s)" %
                         (datetime.datetime.now(), search_info, login, 'search_by_status()'))

        except:
            logging.warning("%s: Error with searching tasks by status, wasn't searched tasks by status "
                            "'%s' by %s (func: %s)" %
                            (datetime.datetime.now(), search_info, login, 'search_by_status()'))


def help_ru():
    try:
        print('''add_task': login, password, header, priority, tags, comment, date_of_start, time_of_start,
                     date_of_end, time_of_end, status),
        'delete_task': (login, password, task_id),
        'change_task_date': (login, password, id, date_of_start, date_of_end, time_of_start, time_of_end),
        'change_expert': (login, password, id, expert),
        'change_status': (login, password, id, status),
        'change_task': (login, password, id, header, priority, tags, comment),
        'add_linked_task': (login, password, id, linked_task_id),
        'delete_link_between_linked_task': (login, password, id, linked_task_id),
        'add_parent_for_task': (login, password, id, parent_id, expert)
        'delete_link_between_task': (login, password, id, parent_id),
        'group_by_status': (login, password),
        'group_by_tags': (login, password),
        'print_archive': (login, password),
        'print_non_archived_tasks': (login, password),
        'search_by_header': (login, password, search_info),
        'search_by_status': (login, password, search_info),
        'search_by_tags': (login, password, search_info),
        'registration': (login, password, question, answer),
        'change_password': (login, password, new_password, question, answer)''')
        logging.info(" %s: Successful function call, was called function '%s' (func: %s)" %
                     (datetime.datetime.now(), 'help_en()', 'help_en()'))
    except:
        logging.warning("%s: Error with function call, wasn't called function '%s' (func: %s) " %
                        (datetime.datetime.now(), 'help_en()', 'help_en()'))


# if __name__ == '__main__':
#     ArchiveTask.group_by_tags('aa', 'aa')
# #     TaskChangeMethods.change_parent_task(10)
# #     TaskAddMethods.add_task('aa', 'aa', 'Здача 3', 'Низкий','Др','Комментарий 3','11-11-2018','16:40','22-11-2018',
# # '16:40', 'Выполняется')
# #     HelpMethods.is_tasks_linked(1,2)
# #     ParentTaskMethods.add_parent_for_task('aa', 'aa', 3, 2, 'aa')
# #     LinkedTaskMethods.add_linked_task('aa', 'aa', 11, 12)
# #     LinkedTaskMethods.delete_link_between_linked_task('aa', 'aa', 10, 11)
# #     ParentTaskMethods.delete_link_between_task('aa','aa', 2, 1)
# #     TaskDeleteMethods.delete_task('aa','aa',3)
# #     LinkedTaskMethods.delete_link_between_linked_task('aa','aa',1,2)
