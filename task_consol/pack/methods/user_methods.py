from sqlalchemy.exc import *
from pack.functions.user_functions import UsersFunctions
from pack.db.tables import User
from pack.methods.task_methods import *


class UserMethods:

    @classmethod
    def registration(cls, login, password, question, answer):
        import datetime
        """
        This function is designed to register new users.
        :param login:  login for registration
        :param password: password to register
        :param question: a secret question for registration, then used to restore a forgotten password
        :param answer: answer to the secret question for registration, then used to restore the forgotten password
        :return: create user in db
        """
        try:
            user = User(login=login, password=password, question=question, answer=answer)
            UsersFunctions.add_user(user)
            logging.info(" %s: Successful registration, was registered user '%s' (func: %s)" %
                         (datetime.datetime.now(), login, 'registration()'))
        except IntegrityError as e:
            exception = e.args[0]
            print(exception[120:])
            logging.warning("%s: Error with registration, wasn't registered user '%s' (func: %s)" %
                            (datetime.datetime.now(), login, 'registration()'))

    @classmethod
    def change_password(cls, login, password, new_password, question, answer):
        """
        this function is for changing the password
        :param question: security question specified during registration
        :param answer: user password for recovery
        :param new_password: new password
        :return: new password
        """
        try:
            user = UsersFunctions.get_user_by_login_and_password(login, password)
            if user is None:
                print("Данного пользователя не существует, проверьте пароль")
            else:
                user = User(login=login, password=password, question=question, answer=answer)
                UsersFunctions.change_user_password(user, new_password)
            logging.info(" %s: Successful changing users password, was changed password by user '%s'(func: %s)" %
                         (datetime.datetime.now(), login, 'change_password()'))
        except:
            logging.warning("%s: Error with changing users password, wasn't changed password by user '%s'(func: %s)" %
                            (datetime.datetime.now(), login, 'change_password()'))
