from pack.methods.task_methods import *
from pack.methods.user_methods import *
import logging
import datetime


logging.basicConfig(filename="journal_info.log", level=logging.INFO)


def add_task(login, password, header, priority, tags, comment, date_of_start, time_of_start,
             date_of_end, time_of_end, status):
    try:
        TaskAddMethods.add_task(login, password, header, priority, tags, comment, date_of_start, time_of_start,
                                date_of_end, time_of_end, status)
        logging.info(" %s: Successful task adding, was created task '%s' by %s (function: %s)" %
                     (datetime.datetime.now(), header, login, 'add_task()'))
    except:
        logging.warning(" %s: Error with task adding, was not created task '%s' by %s (function: %s)" %
                        (datetime.datetime.now(), header, login, 'add_task()'))


def delete_task(login, password, task_id):
    try:
        TaskDeleteMethods.delete_task(login, password, task_id)
        logging.info(" %s: Successful task deleting, was deleted task with id '%s' by %s (function: %s)" %
                     (datetime.datetime.now(), task_id, login, 'delete_task()'))
    except:
        logging.warning(" %s: Error with task deleting, was not deleted task with id '%s' by %s (function: %s)" %
                        (datetime.datetime.now(), task_id, login, 'delete_task()'))


def change_task_date(login, password, task_id, date_of_start, date_of_end, time_of_start, time_of_end):
    try:
        TaskChangeMethods.change_task_date(login, password, task_id, date_of_start, date_of_end, time_of_start,
                                           time_of_end)
        logging.info(" %s: Successful changing task date, was changed date of task with id '%s' by %s (function: %s)" %
                     (datetime.datetime.now(), task_id, login, 'change_task_date()'))
    except:
        logging.warning("%s: Error with changing task date, was not changed date of task with id '%s' by %s (func: %s)"
                        % (datetime.datetime.now(), task_id, login, 'change_task_date()'))


def change_expert(login, password, task_id, expert):
    try:
        TaskChangeMethods.change_expert(login, password, task_id, expert)
        logging.info(" %s: Successful changing task expert, was changed expert of task with id '%s' by %s (func: %s)" %
                     (datetime.datetime.now(), task_id, login, 'change_expert()'))
    except:
        logging.warning("%s: Error with changing task expert, wasn't changed expert of task: id '%s' by %s (func: %s)" %
                        (datetime.datetime.now(), task_id, login, 'change_expert()'))


def change_status(login, password, task_id, status):
    try:
        TaskChangeMethods.change_status(login, password, task_id, status)
        logging.info(" %s: Successful changing task status, was changed status of task with id '%s' by %s (func: %s)" %
                     (datetime.datetime.now(), task_id, login, 'change_status()'))
    except:
        logging.warning("%s: Error with changing task status, wasn't changed status of task: id '%s' by %s (func: %s)" %
                        (datetime.datetime.now(), task_id, login, 'change_status()'))


def change_task(login, password, task_id, header, priority, tags, comment):
    try:
        TaskChangeMethods.change_task(login, password, task_id, header, priority, tags, comment)
        logging.info(" %s: Successful changing task, was changed task with id '%s' by %s (func: %s)" %
                     (datetime.datetime.now(), task_id, login, 'change_task()'))
    except:
        logging.warning("%s: Error with changing task, wasn't changed task: id '%s' by %s (func: %s)" %
                        (datetime.datetime.now(), task_id, login, 'change_task()'))


def add_linked_task(login, password, task_id, linked_task_id):
    try:
        LinkedTaskMethods.add_linked_task(login, password, task_id, linked_task_id)
        logging.info(" %s: Successful adding linked task, was  added linked task with id '%s' by %s (func: %s)" %
                     (datetime.datetime.now(), task_id, login, 'add_linked_task()'))
    except:
        logging.warning("%s: Error with adding linked task, wasn't added task: id '%s' by %s (func: %s)" %
                        (datetime.datetime.now(), task_id, login, 'add_linked_task()'))


def delete_link_between_linked_task(login, password, task_id, linked_task_id):
    try:
        LinkedTaskMethods.delete_link_between_linked_task(login, password, task_id, linked_task_id)
        logging.info(" %s: Successful deleting of link, was not deleting linked task with id '%s' by %s (func: %s)" %
                     (datetime.datetime.now(), task_id, login, 'delete_link_between_linked_task()'))
    except:
        logging.warning("%s: Error with deleting link, wasn't deleted tasks link: id '%s' by %s (func: %s)" %
                        (datetime.datetime.now(), task_id, login, 'delete_link_between_linked_task()'))


def add_parent_for_task(login, password, task_id, parent_id, expert):
    try:
        ParentTaskMethods.add_parent_for_task(login, password, task_id, parent_id, expert)
        logging.info(" %s: Successful adding parent for task, was added parent for task with id '%s' by %s (func: %s)" %
                     (datetime.datetime.now(), task_id, login, 'add_parent_for_task()'))
    except:
        logging.warning("%s: Error with adding parent for task, wasn't added parent for task: id '%s' "
                        "by %s (func: %s)" %
                        (datetime.datetime.now(), task_id, login, 'add_parent_for_task()'))


def delete_link_between_task(login, password, task_id, parent_id):
    try:
        ParentTaskMethods.delete_link_between_task(login, password, task_id, parent_id)
        logging.info(" %s: Successful deleting of link, was deleting linked task with id '%s' by %s (func: %s)" %
                     (datetime.datetime.now(), task_id, login, 'delete_link_between_task()'))
    except:
        logging.warning("%s: Error with deleting link, wasn't deleted tasks link: id '%s' by %s (func: %s)" %
                        (datetime.datetime.now(), task_id, login, 'delete_link_between_task()'))


def group_by_status(login, password):
    try:
        ArchiveTask.group_by_status(login, password)
        logging.info(" %s: Successful grouping tasks by status, was grouped tasks by status by %s (func: %s)" %
                     (datetime.datetime.now(), login, 'group_by_status()'))
    except:
        logging.warning("%s: Error with grouping tasks by status, wasn't grouping tasks by %s (func: %s)" %
                        (datetime.datetime.now(), login, 'group_by_status()'))


def group_by_tags(login, password):
    try:
        ArchiveTask.group_by_tags(login, password)
        logging.info(" %s: Successful grouping tasks by tags, was grouped tasks by tags by %s (func: %s)" %
                     (datetime.datetime.now(), login, 'group_by_tags()'))
    except:
        logging.warning("%s: Error with grouping tasks by tags, wasn't grouping tasks by %s (func: %s)" %
                        (datetime.datetime.now(), login, 'group_by_tags()'))


def print_archive(login, password):
    try:
        ArchiveTask.print_archive(login, password)
        logging.info(" %s: Successful printing archive, was printed by %s (func: %s)" %
                     (datetime.datetime.now(), login, 'change_expert()'))
    except:
        logging.warning("%s: Error with archive printing, wasn't printed archives by %s (func: %s)" %
                        (datetime.datetime.now(), login, 'print_archive()'))


def print_non_archived_tasks(login, password):
    try:
        ArchiveTask.print_non_archived_tasks(login, password)
        logging.info(" %s: Successful printing non archive tasks, was printed by %s (func: %s)" %
                     (datetime.datetime.now(), login, 'print_non_archived_tasks()'))
    except:
        logging.warning("%s: Error with non archive printing, wasn't printed non archives tasks by %s (func: %s)" %
                        (datetime.datetime.now(), login, 'print_archive()'))


def search_by_header(login, password, search_info):
    try:
        ArchiveTask.search_by_header(login, password, search_info)
        logging.info(" %s: Successful searching tasks by header, was searched tasks by header '%s' by %s (func: %s)" %
                     (datetime.datetime.now(), search_info, login, 'search_by_header()'))
    except:
        logging.warning("%s: Error with searching tasks by header, wasn't searched tasks by header "
                        "'%s' by %s (func: %s)" %
                        (datetime.datetime.now(), search_info, login, 'search_by_header()'))


def search_by_status(login, password, search_info):
    try:
        ArchiveTask.search_by_status(login, password, search_info)
        logging.info(" %s: Successful searching tasks by status, was searched tasks by status '%s' by %s (func: %s)" %
                     (datetime.datetime.now(), search_info, login, 'search_by_status()'))

    except:
        logging.warning("%s: Error with searching tasks by status, wasn't searched tasks by status "
                        "'%s' by %s (func: %s)" %
                        (datetime.datetime.now(), search_info, login, 'search_by_status()'))


def search_by_tags(login, password, search_info):
    try:
        ArchiveTask.search_by_tags(login, password, search_info)
        logging.info(" %s: Successful searching tasks by tags, was searched tasks by tags '%s' by %s (func: %s)" %
                     (datetime.datetime.now(), search_info, login, 'search_by_tags()'))
    except:
        logging.warning("%s: Error with searching tasks by tags, wasn't searched tasks by tags "
                        "'%s' by %s (func: %s)" %
                        (datetime.datetime.now(), search_info, login, 'search_by_tags()'))


def help_en():
    try:
        help_ru()
        logging.info(" %s: Successful function call, was called function '%s' (func: %s)" %
                     (datetime.datetime.now(), 'help_en()', 'help_en()'))
    except:
        logging.warning("%s: Error with function call, wasn't called function '%s' (func: %s) " %
                        (datetime.datetime.now(), 'help_en()', 'help_en()'))


def registration(login, password, question, answer):
    try:
        UserMethods.registration(login, password, question, answer)
        logging.info(" %s: Successful registration, was registered user '%s' (func: %s)" %
                     (datetime.datetime.now(), login, 'registration()'))
    except:
        logging.warning("%s: Error with registration, wasn't registered user '%s' (func: %s)" %
                        (datetime.datetime.now(), login, 'registration()'))


def change_password(login, password, new_password, question, answer):
    try:
        UserMethods.change_password(login, password, new_password, question, answer)
        logging.info(" %s: Successful changing users password, was changed password by user '%s'(func: %s)" %
                     (datetime.datetime.now(), login, 'change_password()'))
    except:
        logging.warning("%s: Error with changing users password, wasn't changed password by user '%s'(func: %s)" %
                        (datetime.datetime.now(), login, 'change_password()'))


