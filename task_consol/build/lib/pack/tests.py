import unittest
from sqlalchemy.exc import *

from pack.methods.task_methods import *
from pack.methods.user_methods import *


class TestsUser(unittest.TestCase):

    def test_user_registration_one(self):
        login = "testere_user"
        password = "test_password"
        question = 'Where is your car?'
        answer = 'at home'

        try:
            user = UserMethods.registration(login, password, question, answer)
            status = True
            UsersFunctions.delete_user(login)
        except :
            status = False
            UsersFunctions.delete_user(login)
        self.assertEqual(status, True)


    def test_user_registration_two(self):
        login = "tester_user"
        log = "tester_user"
        password = "test_password"
        question = 'Where is your car?'
        answer = 'at home'

        # user = UserMethods.registration(log, password, question, answer)

        try:
            userw = UserMethods.registration(login, password, question, answer)
            status = True
            UsersFunctions.delete_user(login)
        except:
            status = False
            # UsersFunctions.delete_user(login)
        finally:
            # UsersFunctions.delete_user(log)
            self.assertEqual(status, True)


    #
    # def test_user_change_password_one(self):
    #     login = "tester_user"
    #     password = "test_password"
    #     new_password = 'new_test_password'
    #     question = 'Where is your car?'
    #     answer = 'at home'
    #
    #     try:
    #         UserMethods.change_password(login, password, new_password, question, answer)
    #         status = True
    #     except:
    #         status = False
    #
    #     self.assertEqual(status, False)
    #
    # def test_user_change_password_two(self):
    #     login = "test_user"
    #     password = "test_password"
    #     new_password = 'new_test_password'
    #     question = 'Where is your car?'
    #     answer = 'at home'
    #     user = UserMethods.registration(login, password, question, answer)
    #
    #     try:
    #         UserMethods.change_password(login, password, new_password, question, answer)
    #         status = True
    #     except:
    #         status = False
    #
    #     self.assertEqual(status, True)