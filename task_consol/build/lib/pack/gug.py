from pack.terminal import *
"""
dictionary for interaction with the console via arg parser
"""
ALL_FUNCTIONS = {
    'add_task': add_task,
    'delete_task': delete_task,
    'change_task_date': change_task_date,
    'change_expert': change_expert,
    'change_status': change_status,
    'change_task': change_task,
    'add_linked_task': add_linked_task,
    'delete_link_between_linked_task': delete_link_between_linked_task,
    'add_parent_for_task': add_parent_for_task,
    'delete_link_between_task': delete_link_between_task,
    'group_by_status': group_by_status,
    'group_by_tags': group_by_tags,
    'print_archive': print_archive,
    'print_non_archived_tasks':print_non_archived_tasks,
    'search_by_header': search_by_header,
    'search_by_status': search_by_status,
    'search_by_tags': search_by_tags,
    'help_en': help_en,
    'registration': registration,
    'change_password': change_password,
}
